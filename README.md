# my.safesurfer.io

Code for the [my.safesurfer.io](https://my.safesurfer.io) user dashboard. See [Dashboard](https://gitlab.com/safesurfer/dashboard) for the reference implementation.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
