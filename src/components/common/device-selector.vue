<template>
  <div
    v-if="value"
    class="device-selector"
  >
    <b-dropdown
      style="margin: 0px;"
      :value="value.id"
      :disabled="disabled"
      @input="assignSelection({ id: $event, mac: '' })"
      :position="disableSelectLifeguardClientDevice ? 'is-bottom-right' : 'is-bottom-left'"
      :scrollable="true"
      :append-to-body="true"
    >
      <button
        class="button is-primary device-dropdown border-radius-left"
        slot="trigger"
        slot-scope="{ active }"
        :class="{ 'border-radius-right': (!lifeguardSelected || disableSelectLifeguardClientDevice) }"
      >
        <!-- Show name of device if one is selected, otherwise placeholder. -->
        <span>{{ deviceMap[value.id] ? deviceMap[value.id].name : (placeholder ? placeholder : 'Select a device') }}</span>
        <b-icon :icon="active ? 'menu-up' : 'menu-down'"></b-icon>
      </button>

      <!-- Placeholder device -->
      <b-dropdown-item
        v-if="placeholder"
        :value="''"
      >
        {{ placeholder }}
      </b-dropdown-item>

      <!-- Show mobile devices if any -->
      <b-dropdown-item
        v-for="device in devices.mobile"
        :key="device.id"
        :value="device.id"
      >
        <div class="media">
          <b-icon
            v-if="device.type"
            class="media-left"
            icon="cellphone"
          />
          <div class="media-content">
            <h3>{{ device.name }}</h3>
          </div>
        </div>
      </b-dropdown-item>

      <!-- Show dns devices if any -->
      <b-dropdown-item
        v-for="device in devices.dns"
        :key="device.id"
        :value="device.id"
      >
        <div class="media">
          <b-icon
            class="media-left"
            icon="dns"
          />
          <div class="media-content">
            <h3>{{ device.name }}</h3>
          </div>
        </div>
      </b-dropdown-item>

      <!-- Show lifeguard devices if any -->
      <b-dropdown-item
        v-for="device in devices.lifeguards"
        :key="device.id"
        :value="device.id"
      >
        <div class="media">
          <b-icon
            class="media-left"
            icon="router-wireless"
          />
          <div class="media-content">
            <h3>{{ device.name }}</h3>
          </div>
        </div>
      </b-dropdown-item>
    </b-dropdown>

    <!-- Another dropdown for a lifeguard device's items if one is selected. -->
    <b-dropdown
      style="margin: 0px;"
      v-if="!disableSelectLifeguardClientDevice && lifeguardSelected"
      :disabled="disabled"
      :value="value.mac"
      @input="assignSelection({ id: value.id, mac: $event })"
    >
      <button
        class="button is-grey device-dropdown border-radius-right"
        slot="trigger"
        slot-scope="{ active }"
        outlined
      >
        <!-- Show name of device if one is selected, otherwise placeholder. -->
        <span>{{ value.mac ? lifeguardDeviceNames[value.id + '/' + value.mac] : lifeguardPlaceholder }}</span>
        <b-icon :icon="active ? 'menu-up' : 'menu-down'"></b-icon>
      </button>

      <!-- Placeholder item -->
      <b-dropdown-item
        :value="''"
      >
        {{ lifeguardPlaceholder }}
      </b-dropdown-item>

      <!-- Client devices -->
      <b-dropdown-item
        v-for="device in lifeguardDevices[value.id]"
        :key="device.mac"
        :value="device.mac"
      >
        {{ device.name }}
      </b-dropdown-item>
    </b-dropdown>
  </div>
</template>

<script lang="ts">
import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Watch } from 'vue-property-decorator'
import { APIDevices, APIDevice } from '@/requests/devices/get-devices'
import clone from 'lodash/clone'
import assign from 'lodash/assign'

export interface DeviceSelection {
  id: string;
  mac: string;
  name?: string;
}

/**
 * Helper class to validate device selection based on input params.
 */
export class DeviceSelectionValidator {

  private devices: APIDevices
  private disableSelectLifeguardClientDevice: boolean
  /**
   * Map from device ID to whether is a lifeguard.
   */
  private isLifeguard: {[id: string]: boolean} = {}

  constructor(devices: APIDevices, disableSelectLifeguardClientDevice: boolean) {
    this.devices = devices
    this.disableSelectLifeguardClientDevice = disableSelectLifeguardClientDevice
    // Init whether each device id is a lifeguard
    if (this.devices.lifeguards) {
      this.devices.lifeguards.forEach(lifeguard => {
        this.isLifeguard[lifeguard.id] = true
      })
    }
    if (this.devices.mobile) {
      this.devices.mobile.forEach(device => {
        this.isLifeguard[device.id] = false
      })
    }
    if (this.devices.dns) {
      this.devices.dns.forEach(device => {
        this.isLifeguard[device.id] = false
      })
    }
  }

  /**
   * @returns Whether the selection would be valid for the devices on this validator.
   */
  selectionIsValid(selection: DeviceSelection): boolean {
    const invalid = this.isLifeguard[selection.id] && !selection.mac && !this.disableSelectLifeguardClientDevice
    return !invalid
  }
}

/**
 * Component to select a device registered to the user.
 */
@Component
export default class DeviceSelector extends Vue {
  /**
   * The devices registered to the user.
   */
  @Prop() readonly devices: APIDevices | undefined
  /**
   * The user's current selection.
   */
  @Prop() readonly value: DeviceSelection | undefined
  /**
   * The placeholder text if no device is selected.
   */
  @Prop() readonly placeholder: string | undefined
  /**
   * The placeholder text if no lifeguard device is selected.
   */
  @Prop() readonly lifeguardPlaceholder: string | undefined
  /**
   * Whether to disable selecting a specific lifeguard device.
   */
  @Prop() readonly disableSelectLifeguardClientDevice: boolean | undefined
  /**
   * Whether to disable the dropdown.
   */
  @Prop() readonly disabled: boolean | undefined

  /**
   * Map from device ID to the device.
   */
  deviceMap: {[id: string]: APIDevice} = {}
  /**
   * Map from concatenated device ID (lifeguard ID + '/' + MAC) to lifeguard device name.
   */
  lifeguardDeviceNames: {[id: string]: string} = {}
  /**
   * Map from lifeguard device ID to a list of its client devices.
   */
  lifeguardDevices: {[id: string]: APIDevice[]} = {}
  /**
   * Map from device ID to whether is a lifeguard.
   */
  isLifeguard: {[id: string]: boolean} = {}

  /**
   * When the devices change, update the vars we use to display them.
   */
  @Watch('props.devices', { deep: true, immediate: true }) updateDisplayDevices () {
    if (!this.devices) {
      return
    }
    this.deviceMap = {}
    this.lifeguardDeviceNames = {}
    // Add lg device names
    if (this.devices.lifeguards) {
      this.devices.lifeguards.forEach(lifeguard => {
        if (lifeguard.devices) {
          lifeguard.devices.forEach(device => {
            this.lifeguardDeviceNames[lifeguard.id + '/' + device.mac] = device.name   
          })
        }
        this.lifeguardDevices[lifeguard.id] = lifeguard.devices
        this.deviceMap[lifeguard.id] = lifeguard
        this.isLifeguard[lifeguard.id] = true
      })
    }
    // Add mobile device names
    if (this.devices.mobile) {
      this.devices.mobile.forEach(device => {
        this.deviceMap[device.id] = device
        this.isLifeguard[device.id] = false
      })
    }
    // Add dns device names
    if (this.devices.dns) {
      this.devices.dns.forEach(device => {
        this.deviceMap[device.id] = device
        this.isLifeguard[device.id] = false
      })
    }
  }

  /**
   * Return whether a lifeguard device is selected.
   */
  get lifeguardSelected (): boolean {
    if (!this.value || !this.value.id) {
      return false
    }
    return this.isLifeguard[this.value.id]
  }

  /**
   * Update the device ID and/or mac of the user's selection, and recalculate
   * whether it's valid.
   */
  assignSelection (selection: DeviceSelection) {
    if (!this.value) {
      return
    }
    // Calculate name
    if (selection.id && selection.mac) {
      selection.name = this.lifeguardDeviceNames[selection.id + '/' + selection.mac]
    } else if (selection.id) {
      selection.name = this.deviceMap[selection.id].name
    }
    const newSelection = assign(clone(this.value), selection)
    this.$emit('input', newSelection)
  }
}
</script>

<style scoped>
.device-dropdown {
  border-radius: 0px;
  margin: 0px 0px 0px 0px;
}

.border-radius-left {
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
}

.border-radius-right {
  border-top-right-radius: 20px;
  border-bottom-right-radius: 20px;
}
</style>
