import { Plan } from "@/requests/user-v2/get-plan"
import cloneDeep from 'lodash/cloneDeep'

export const TOKEN_STORAGE_NAME = 'my-ss-token'
export const EMAIL_STORAGE_NAME = 'my-ss-user'
export const DISMISSED_ANNOUNCEMENTS_STORAGE_NAME = 'my-ss-dismissed-announcements'
export const BASE_URL_STAGING = 'https://staging.api.safesurfer.io'
export const BASE_URL_STAGING_IPV6 = 'https://staging-ipv6.api.safesurfer.io'
export const CHARGEBEE_SITE_STAGING = 'safesurfer-test'
export const KNOX_API_URL_STAGING = 'https://staging.knox-api.safesurfer.io'
export const KNOX_API_URL = "https://knox-api.safesurfer.io"

/**
 * Describes a way of setting up Safe Surfer.
 */
export interface SetupOption {
  title: string;
  icon: string;
  description?: string;
  /**
   * Either one of below must be specified.
   * Route is a Vue route object.
   */
  children?: SetupOption[];
  route?: any;
  link?: string;
  tags?: SetupOptionTag[];
  componentName?: string;
}
  
export interface SetupOptionTag {
  type: string;
  text: string;
}

export const ANDROID_APP_SETUP_CONFIG: SetupOption = {
  title: 'Get the Android app',
  icon: 'google-play',
  description: 'Download the app and register the device to your account',
  children: [
    {
      title: 'Android App',
      icon: '',
      componentName: 'android-app-setup'
    },
  ],
  tags: [
    {
      type: 'is-success',
      text: 'Easy'
    }
  ]
}

export const ANDROID_KID_SAFE_PHONE_INSTORE: SetupOption = {
  title: 'Buy in-store',
  icon: 'store-outline',
  description: 'Buy a Kid-Safe Smartphone in-store.',
  link: 'https://safesurfer.io/kidsafephone',
  tags: [
    {
      type: 'is-success',
      text: 'NZ Only'
    }
  ]
}

export const ANDROID_KID_SAFE_TABLET_INSTORE: SetupOption = {
  title: 'Buy in-store',
  icon: 'store-outline',
  description: 'Buy a Kid-Safe Tablet in-store.',
  link: 'https://safesurfer.io/kidsafetablet',
  tags: [
    {
      type: 'is-success',
      text: 'NZ Only'
    }
  ]
}

export const ANDROID_KID_SAFE_PHONE_NEW: SetupOption = {
  title: 'Set up a new device',
  icon: 'package',
  description: 'Set up a Samsung device out of the box. Active annual subscription required. An extra per-device charge applies.',
  children: [
    {
      title: 'New Kid-Safe Smartphone',
      icon: '',
      componentName: 'new-kid-safe-phone-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Knox required'
    }
  ]
}

export const ANDROID_KID_SAFE_TABLET_NEW: SetupOption = {
  title: 'Set up a new device',
  icon: 'package',
  description: 'Set up a Samsung device out of the box. Active annual subscription required. An extra per-device charge applies.',
  children: [
    {
      title: 'New Kid-Safe Tablet',
      icon: '',
      componentName: 'new-kid-safe-tablet-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Knox required'
    }
  ]
}

export const ANDROID_KID_SAFE_PHONE_EXISTING: SetupOption = {
  title: 'Set up an existing device',
  icon: 'cellphone-cog',
  description: 'Set up an existing Samsung device. Active annual subscription required. An extra per-device charge applies.',
  children: [
    {
      title: 'Convert to Kid-Safe Smartphone',
      icon: '',
      componentName: 'existing-kid-safe-phone-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Factory reset required'
    },
    {
      type: 'is-warning',
      text: 'Android 11 or newer with Knox'
    },
  ]
}

export const ANDROID_KID_SAFE_TABLET_EXISTING: SetupOption = {
  title: 'Set up an existing device',
  icon: 'cellphone-cog',
  description: 'Set up an existing Samsung device. Active annual subscription required. An extra per-device charge applies.',
  children: [
    {
      title: 'Convert to Kid-Safe Tablet',
      icon: '',
      componentName: 'existing-kid-safe-tablet-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Factory reset required'
    },
    {
      type: 'is-warning',
      text: 'Android 11 or newer with Knox'
    },
  ]
}

export const ANDROID_KID_SAFE_PHONE_SETUP_CONFIG: SetupOption = {
  title: 'Kid-Safe Smartphone',
  icon: 'cellphone-lock',
  description: 'A locked down safe phone for kids, in collaboration with Samsung',
  children: [], // important
  tags: [
    {
      type: 'is-success',
      text: 'Extra features'
    }
  ]
}

export const ANDROID_KID_SAFE_TABLET_SETUP_CONFIG: SetupOption = {
  title: 'Kid-Safe Tablet',
  icon: 'tablet',
  description: 'A locked down safe tablet for kids, in collaboration with Samsung',
  children: [], // important
  tags: [
    {
      type: 'is-success',
      text: 'Extra features'
    }
  ]
}

export const ANDROID_PRIVATE_DNS_SETUP_CONFIG: SetupOption = {
  title: 'Change settings',
  icon: 'cellphone-cog',
  description: 'Install filtering without downloading the Safe Surfer app',
  children: [
    {
      title: 'Android Private DNS Setup',
      icon: '',
      componentName: 'android-private-dns-setup'
    },
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'Android 9 or newer'
    }
  ]
}

export const IOS_APP_SETUP_CONFIG: SetupOption = {
  title: 'Get the iPhone/iPad app',
  icon: 'apple',
  description: 'Download the app and register the device to your account',
  children: [
    {
      title: 'iOS App',
      icon: '',
      componentName: 'ios-app-setup'
    }
  ],
  tags: [
    {
      type: 'is-success',
      text: 'Easy'
    },
    {
      type: 'is-warning',
      text: 'iOS/iPadOS 16 or newer'
    }
  ]
}

export const IOS_FILTER_SETTINGS_SETUP_CONFIG: SetupOption = {
  title: 'Apply filter settings',
  icon: 'shield-link-variant',
  description: 'Install filtering without downloading the Safe Surfer app',
  children: [
    {
      title: 'Apply iOS Filtering',
      icon: '',
      componentName: 'apple-mobileconfig-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'iOS/iPadOS 14 or newer'
    },
  ]
}

export const WINDOWS_DOH_APP_SETUP_CONFIG: SetupOption = {
  title: 'Get the Windows app',
  icon: 'microsoft',
  description: 'Download the app and register the device to your account',
  children: [
    {
      title: 'Windows App',
      icon: '',
      componentName: 'windows-app-setup'
    }
  ],
  tags: [
    {
      type: 'is-success',
      text: 'Easy'
    },
  ]
}

export const WINDOWS_DNS_SETUP_CONFIG: SetupOption = {
  title: 'Use the DNS',
  icon: 'wrench',
  description: 'Switch to Safe Surfer DNS manually. Link your IP to apply custom account rules.',
  route: {
    name: 'DeviceSetup',
    query: {
      modal: 'windows-dns-setup',
      'setup-name': 'Windows DNS Setup'
    }
  },
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    }
  ]
}

export const MACOS_FILTER_SETTINGS_SETUP_CONFIG: SetupOption = {
  title: 'Apply filter settings',
  icon: 'shield-link-variant',
  description: 'Install Safe Surfer filtering',
  children: [
    {
      title: 'Apply iOS Filtering',
      icon: '',
      componentName: 'mac-mobileconfig-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'macOS Big Sur (11) or newer'
    },
  ]
}

export const CHROME_SETUP_CONFIG: SetupOption = {
  title: 'Google Chrome',
  icon: 'google-chrome',
  description: 'Protect an individual Chrome browser app',
  children: [
    {
      title: 'Chrome',
      icon: 'chrome',
      componentName: 'chrome-dns-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'Android, laptop, and desktop only'
    },
  ]
}

export const CHROMEOS_SETUP_CONFIG: SetupOption = {
  title: 'Apply filter settings',
  icon: 'shield-link-variant',
  description: 'Install Safe Surfer filtering',
  children: [
    {
      title: 'Chromebook',
      icon: '',
      componentName: 'chromeos-dns-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
  ]
}

export const EDGE_SETUP_CONFIG: SetupOption = {
  title: 'Microsoft Edge',
  icon: 'microsoft-edge',
  description: 'Protect an individual Edge browser app',
  children: [
    {
      title: 'Edge',
      icon: '',
      componentName: 'edge-dns-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'Laptop and desktop only'
    },
  ]
}

export const FIREFOX_SETUP_CONFIG: SetupOption = {
  title: 'Firefox',
  icon: 'firefox',
  description: 'Protect an individual Firefox browser app',
  children: [
    {
      title: 'Firefox',
      icon: '',
      componentName: 'firefox-dns-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'Laptop and desktop only'
    },
  ]
}

export const LIFEGUARD_SETUP_CONFIG: SetupOption = {
  title: 'Get a Lifeguard router',
  icon: 'shield-home',
  description: 'Safe Surfer\'s custom Wi-Fi router is particularly useful for specific tasks, such as YouTube filtering on smart TVs and video game consoles, or protecting Chromebooks logged in to a school account. If you don\'t need to protect these kinds of devices, we recommend using our protection apps instead, as they feature additional protection technology',
  link: 'https://shop.safesurfer.io/collections/resources/products/internet-lifeguard',
  tags: [
    {
      type: 'is-warning',
      text: 'Requires existing modem/router'
    },
    {
      type: 'is-warning',
      text: 'AU/NZ only'
    },
  ]
}

export const EXISTING_ROUTER_SETUP_CONFIG: SetupOption = {
  title: 'Protect my existing router',
  icon: 'wrench',
  description: 'Change your Wi-Fi router settings to protect your local network',
  children: [
    {
      title: 'Router Settings',
      icon: '',
      componentName: 'router-dns-setup'
    }
  ],
  tags: [
    {
      type: 'is-warning',
      text: 'Manual setup'
    },
    {
      type: 'is-warning',
      text: 'Can be difficult or unsupported'
    },
  ]
}

/**
   * @returns The OS that we think this device is.
   */
export function getOS (): 'Windows' | 'macOS' | 'Linux' | 'Android' | 'iOS' | 'Unknown' | 'ChromeOS' {
  const userAgent = navigator.userAgent
  if (userAgent.indexOf("Win") != -1) return 'Windows'
  if (userAgent.indexOf("like Mac") != -1) return 'iOS'
  if (userAgent.indexOf("Mac") != -1) return 'macOS'
  if (userAgent.indexOf("Android") != -1) return 'Android'
  if (userAgent.indexOf("Linux") != -1) return 'Linux'
  if (userAgent.indexOf("CrOS") != -1) return 'ChromeOS'
  return 'Unknown'
}

function getRecommendedOptions(): SetupOption[] {
  const recc: SetupOption[] = []
    switch (getOS()) {
      case 'Windows':
        recc.push(WINDOWS_DOH_APP_SETUP_CONFIG)
        break
      case 'macOS':
        recc.push(MACOS_FILTER_SETTINGS_SETUP_CONFIG)
        break
      case 'ChromeOS':
        recc.push(CHROMEOS_SETUP_CONFIG)
        break
      case 'Linux':
        // recc.push('desktop-app-option')
        break
      case 'Android':
        recc.push(ANDROID_APP_SETUP_CONFIG, ANDROID_PRIVATE_DNS_SETUP_CONFIG)
        break
      case 'iOS':
        recc.push(IOS_APP_SETUP_CONFIG, IOS_FILTER_SETTINGS_SETUP_CONFIG)
        break
    }
    // If there are no recommendations, show home wifi protection as a backup
    if (recc.length === 0) {
      recc.push(LIFEGUARD_SETUP_CONFIG, EXISTING_ROUTER_SETUP_CONFIG)
    }
    return recc
}

export function getPromoSetupOptions(): SetupOption[] {
  const phoneConfig = cloneDeep(ANDROID_KID_SAFE_PHONE_SETUP_CONFIG)
  phoneConfig.children?.push(ANDROID_KID_SAFE_PHONE_NEW, ANDROID_KID_SAFE_PHONE_EXISTING)
  const tabletConfig = cloneDeep(ANDROID_KID_SAFE_TABLET_SETUP_CONFIG)
  tabletConfig.children?.push(ANDROID_KID_SAFE_TABLET_NEW, ANDROID_KID_SAFE_TABLET_EXISTING)
  return [
    {
      title: 'New Kid-Safe device',
      icon: 'cellphone-lock',
      description: 'Register another Kid-Safe device',
      children: [
        phoneConfig,
        tabletConfig
      ]
    },
    {
      title: 'Other device types',
      icon: 'plus',
      description: 'Upgrade to Pro Surfer to protect your Wi-Fi and register other Android/Apple/Windows devices.',
      route: {
        name: 'KsePromoSwitch'
      }
    }
  ]
}

export function getAllSetupOptions(plan: Plan | null, ksp: boolean): SetupOption[] {
  const android = [
    ANDROID_APP_SETUP_CONFIG,
    ANDROID_PRIVATE_DNS_SETUP_CONFIG
  ]
  if (ksp && plan && plan.id.includes('NZ')) {
    const phoneConfig = cloneDeep(ANDROID_KID_SAFE_PHONE_SETUP_CONFIG)
    // if (plan && plan.id.includes('NZ')) {
    //   phoneConfig.children?.push(ANDROID_KID_SAFE_PHONE_INSTORE)
    // }
    phoneConfig.children?.push(ANDROID_KID_SAFE_PHONE_NEW, ANDROID_KID_SAFE_PHONE_EXISTING)
    android.push(phoneConfig)
    const tabletConfig = cloneDeep(ANDROID_KID_SAFE_TABLET_SETUP_CONFIG)
    // if (plan && plan.id.includes('NZ')) {
    //   tabletConfig.children?.push(ANDROID_KID_SAFE_TABLET_INSTORE)
    // }
    tabletConfig.children?.push(ANDROID_KID_SAFE_TABLET_NEW, ANDROID_KID_SAFE_TABLET_EXISTING)
    android.push(tabletConfig)
  }
  return [
    {
      title: 'This device',
      icon: 'tooltip-account',
      description: 'Protect this device for on the go protection',
      children: getRecommendedOptions()
    },
    {
      title: 'Another mobile, laptop, or desktop device',
      icon: 'cellphone-link',
      description: 'Protect any device for on the go protection',
      children: [
        {
          title: 'Android',
          icon: 'android',
          description: 'Protect an Android mobile device (Samsung, Pixel etc.)',
          children: android
        },
        {
          title: 'iPhone and iPad',
          icon: 'apple',
          description: 'Protect an iOS/iPadOS mobile device',
          children: [
            IOS_APP_SETUP_CONFIG,
            IOS_FILTER_SETTINGS_SETUP_CONFIG
          ]
        },
        {
          title: 'Windows',
          icon: 'microsoft',
          description: 'Protect a Windows laptop or desktop device',
          children: [
            WINDOWS_DOH_APP_SETUP_CONFIG
          ]
        },
        {
          title: 'Mac',
          icon: 'apple-finder',
          description: 'Protect a macOS laptop or desktop device',
          children: [
            MACOS_FILTER_SETTINGS_SETUP_CONFIG
          ]
        },
        {
          title: 'Chromebook and Chromebox',
          icon: 'google-chrome',
          description: 'Protect a ChromeOS laptop or desktop device',
          children: [
            CHROMEOS_SETUP_CONFIG
          ]
        },
        {
          title: 'Web browser',
          icon: 'web',
          description: 'Protect an individual browser app',
          children: [
            CHROME_SETUP_CONFIG,
            EDGE_SETUP_CONFIG,
            FIREFOX_SETUP_CONFIG
          ]
        },
        {
          title: 'Custom',
          icon: 'tools',
          description: 'Protect a custom DNS device (useful for experts)',
          children: [
            {
              title: 'Advanced',
              icon: '',
              componentName: 'advanced-dns-setup'
            }
          ]
        }
      ]
    },
    {
      title: 'Wi-Fi and network',
      icon: 'home-automation',
      description: 'Protect all devices on a local network',
      children: [
        LIFEGUARD_SETUP_CONFIG,
        EXISTING_ROUTER_SETUP_CONFIG
      ]
    },
  ]
}

// Name/icon for common android app IDs.
export const COMMON_APPS = {
  'com.facebook.orca': {
    name: 'Facebook Messenger',
    icon: 'facebook-messenger'
  },
  'com.facebook.mlite': {
    name: 'Facebook Messenger',
    icon: 'facebook-messenger'
  },
  'com.facebook.katana': {
    name: 'Facebook',
    icon: 'facebook'
  },
  'com.facebook.lite': {
    name: 'Facebook',
    icon: 'facebook'
  },
  'com.facebook.talk': {
    name: 'Facebook Messenger Kids',
    icon: 'facebook-messenger'
  },
  'com.twitter.android': {
    name: 'Twitter',
    icon: 'twitter'
  },
  'com.instagram.android': {
    name: 'Instagram',
    icon: 'instagram'
  },
  'com.whatsapp': {
    name: 'WhatsApp',
    icon: 'whatsapp'
  },
  'com.discord': {
    name: 'Discord',
    icon: 'discord'
  },
  'com.snapchat.android': {
    name: 'Snapchat',
    icon: 'snapchat'
  },
  'com.sec.android.app.sbrowser': {
    name: 'Samsung Internet',
    icon: 'web'
  }
}

export interface BlockingProfileRestrictionSetting {
  id: number;
  action: number;
}

export interface BlockingProfile {
  blockedCategories: number[];
  unblockedCategories?: number[];
  remove?: number[];
  restrictionSettings: BlockingProfileRestrictionSetting[];
  include: BlockingProfile[];
}

export const PROFILE_FAMILY_MINIMAL: BlockingProfile = {
  blockedCategories: [41],
  restrictionSettings: [],
  include: []
}

export const PROFILE_FAMILY_SAFE_SURFING: BlockingProfile = {
  blockedCategories: [59, 139, 225, 39, 226, 105, 121],
  restrictionSettings: [],
  include: [ PROFILE_FAMILY_MINIMAL ]
}

export const PROFILE_FAMILY_CLEAN_SURFING: BlockingProfile = {
  blockedCategories: [1],
  restrictionSettings: [],
  include: [ PROFILE_FAMILY_SAFE_SURFING ]
}

export const PROFILE_FAMILY_SUPER_SAFE_SURFING: BlockingProfile = {
  blockedCategories: [146, 178, 128, 188, 593, 436, 180, 555, 61, 11, 20, 346, 122, 428],
  restrictionSettings: [
    {
      id: 145,
      action: 2
    },
    {
      id: 432,
      action: 1
    },
    {
      id: 431,
      action: 1
    },
    {
      id: 433,
      action: 1
    },
    {
      id: 434,
      action: 1
    },
    {
      id: 589,
      action: 1
    },
  ],
  include: [ PROFILE_FAMILY_SAFE_SURFING ]
}

export const PROFILE_KSP_BASIC: BlockingProfile = {
  blockedCategories: [ 437, 277, 126, 130, 237, 438, 142, 127, 131, 132 ],
  restrictionSettings: [
    {
      id: 145,
      action: 3
    }
  ],
  include: [ PROFILE_FAMILY_SUPER_SAFE_SURFING ]
}

export const PROFILE_KSP_INTERMEDIATE: BlockingProfile = {
  blockedCategories: [ 437, 277, 126, 130, 237, 438, 142, 127, 131, 132 ],
  unblockedCategories: [ 346 ], // Potentially unsafe services blocks stuff like Wikipedia images, don't enable by default
  restrictionSettings: [
    {
      id: 145,
      action: 3
    }
  ],
  include: [ PROFILE_FAMILY_SUPER_SAFE_SURFING ]
}

export const PROFILE_INDIVIDUAL_MINIMAL: BlockingProfile = {
  blockedCategories: [41, 225, 226, 105, 121],
  restrictionSettings: [],
  include: []
}

export const PROFILE_INDIVIDUAL_CLEAN_SURFING: BlockingProfile = {
  blockedCategories: [1, 39],
  restrictionSettings: [],
  include: [ PROFILE_INDIVIDUAL_MINIMAL ]
}

export const PROFILE_INDIVIDUAL_PORN_ADDICTION: BlockingProfile = {
  blockedCategories: [146, 139, 178, 128, 188, 593, 436],
  restrictionSettings: [],
  include: [ PROFILE_INDIVIDUAL_CLEAN_SURFING ]
}

export const PROFILE_INDIVIDUAL_FOCUS: BlockingProfile = {
  blockedCategories: [129, 277, 180, 126, 130, 237, 142, 178, 127, 113, 131, 132, 128, 184, 159, 228, 181, 227, 189, 183, 229, 233, 232, 230, 235, 185, 234, 187, 243, 186],
  restrictionSettings: [],
  include: [ PROFILE_INDIVIDUAL_PORN_ADDICTION ]
}

export const PROFILE_BUSINESS_MINIMAL: BlockingProfile = {
  blockedCategories: [41],
  restrictionSettings: [],
  include: []
}

export const PROFILE_BUSINESS_SAFE_SURFING: BlockingProfile = {
  blockedCategories: [225, 226, 105, 121, 39, 59, 20, 139],
  restrictionSettings: [],
  include: [ PROFILE_BUSINESS_MINIMAL ]
}

export const PROFILE_BUSINESS_PRODUCTIVITY: BlockingProfile = {
  blockedCategories: [113, 131, 132, 128, 129, 277, 180, 126, 130, 237, 142, 178, 127 ],
  restrictionSettings: [],
  include: [ PROFILE_BUSINESS_SAFE_SURFING ]
}

export const PROFILE_SCHOOL_MINIMAL: BlockingProfile = {
  blockedCategories: [41],
  restrictionSettings: [],
  include: []
}

export const PROFILE_SCHOOL_SAFE_SURFING: BlockingProfile = {
  blockedCategories: [225, 226, 105, 121, 39, 59, 20, 139],
  restrictionSettings: [],
  include: [ PROFILE_SCHOOL_MINIMAL ]
}

export const PROFILE_SCHOOL_FOCUS: BlockingProfile = {
  blockedCategories: [113, 131, 132, 128, 129, 277, 180, 126, 130, 237, 142, 178, 127 ],
  restrictionSettings: [],
  include: [ PROFILE_SCHOOL_SAFE_SURFING ]
}

export type UserType = 'family' | 'individual' | 'business' | 'school' | ''

export const USER_TYPE_PROMPTS: {[userType: string]: string} = {
  'family': 'Protect your family from harmful online content',
  'individual': 'Recover your digital health',
  'business': 'Keep your employees safe and reduce risk from cyberattacks',
  'school': 'Keep kids safe on the net at school'
}

const EXTRA_DEVICES_ADDON_DISPLAY = {
  title: 'Seats/Devices',
  description: 'Number of devices or people in your organization. When multiple people share a device through e.g. a router installation, the seat limit applies.',
  bottomLink: 'https://safesurfer.io/contact',
  bottomLinkText: 'Need more? Contact us.',
  icon: 'account-network',
  expandDefault: true,
  baseQuota: 'devices'
}

const FAMILY_EXTRA_DEVICES_ADDON_DISPLAY = {
  title: 'Devices',
  description: 'Number of devices you want to protect.',
  bottomLink: 'https://safesurfer.io/contact',
  bottomLinkText: 'Need more? Contact us.',
  icon: 'cellphone',
  expandDefault: true,
  baseQuota: 'devices'
}

const SECURITY_ALERTS_ADDON_DISPLAY = {
  title: 'Security Alerts',
  description: 'The amount of devices online per hour to monitor for security alerts. You can check your usage from the Security Alerts page.',
  bottomLink: 'https://alphasoc.com',
  bottomLinkText: 'Security alerts by AlphaSOC',
  icon: 'bug',
  baseQuota: 'security_alert_endpoints_per_hour'
}

const KID_SAFE_PHONE_ADDON_DISPLAY = {
  title: 'Kid-Safe Devices (Basic)',
  description: 'A locked down safe device for kids, in collaboration with Samsung',
  bottomLink: 'https://safesurfer.io/kidsafephone',
  bottomLinkText: 'Learn More',
  icon: 'cellphone-lock',
  baseQuota: 'kid_safe_phone',
  unitOfMeasure: 'Devices'
}

const KID_SAFE_PHONE_PRO_ADDON_DISPLAY = {
  title: 'Kid-Safe Devices (Pro)',
  description: 'Access to all profiles for Kid-Safe devices.',
  bottomLink: 'https://safesurfer.io/kidsafephone',
  bottomLinkText: 'Learn More',
  icon: 'cellphone-lock',
  baseQuota: 'kid_safe_phone_pro',
  unitOfMeasure: 'Devices'
}

export const ADDON_DISPLAY: {[id: string]: any} = {
  'business-annual_extradevices_NZ': EXTRA_DEVICES_ADDON_DISPLAY,
  'business-annual_extradevices_AU': EXTRA_DEVICES_ADDON_DISPLAY,
  'business-annual_extradevices_US': EXTRA_DEVICES_ADDON_DISPLAY,
  'business_extradevices_NZ': EXTRA_DEVICES_ADDON_DISPLAY,
  'business_extradevices_US': EXTRA_DEVICES_ADDON_DISPLAY,
  'business_extradevices_AU': EXTRA_DEVICES_ADDON_DISPLAY,
  'pro_extradevices_NZ': FAMILY_EXTRA_DEVICES_ADDON_DISPLAY,
  'pro_extradevices_US': FAMILY_EXTRA_DEVICES_ADDON_DISPLAY,
  'pro_extradevices_AU': FAMILY_EXTRA_DEVICES_ADDON_DISPLAY,
  'security-alerts_NZ': SECURITY_ALERTS_ADDON_DISPLAY,
  'security-alerts-annual_NZ': SECURITY_ALERTS_ADDON_DISPLAY,
  'ksp_NZ': KID_SAFE_PHONE_ADDON_DISPLAY,
  'ksp_AU': KID_SAFE_PHONE_ADDON_DISPLAY,
  'ksp_US': KID_SAFE_PHONE_ADDON_DISPLAY,
  'kspp_NZ': KID_SAFE_PHONE_PRO_ADDON_DISPLAY,
  'kspp_AU': KID_SAFE_PHONE_PRO_ADDON_DISPLAY,
  'kspp_US': KID_SAFE_PHONE_PRO_ADDON_DISPLAY,
  'kid-safe-phone': KID_SAFE_PHONE_ADDON_DISPLAY,
  'kid-safe-phone-pro': KID_SAFE_PHONE_PRO_ADDON_DISPLAY,
  'kspp-2_NZ': KID_SAFE_PHONE_ADDON_DISPLAY
}

export const QUOTA_DISPLAY = {
  'devices': {
    maxValue: 650
  },
  'security_alert_endpoints_per_hour': {
    maxValue: 200
  },
  'kid_safe_phone': {
    maxValue: 20
  },
  'kid_safe_phone_pro': {
    maxValue: 20
  }
}
