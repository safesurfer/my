import startCase from 'lodash/startCase'
import toLower from 'lodash/toLower'
import assign from 'lodash/assign'
import shuffle from 'lodash/shuffle'
import { TOKEN_STORAGE_NAME, BASE_URL_STAGING, CHARGEBEE_SITE_STAGING, BASE_URL_STAGING_IPV6, KNOX_API_URL_STAGING, KNOX_API_URL, BlockingProfile, PROFILE_KSP_INTERMEDIATE, PROFILE_FAMILY_SUPER_SAFE_SURFING, PROFILE_KSP_BASIC } from '@/helpers/constants'
import { TimeBasedRules } from '@/requests/categories-v2/post-time-based-category-ruleset'
import { Location, RawLocation, Route } from 'vue-router'
import { APIDevice } from '@/requests/devices/get-devices'
import GetCategoriesRules, { CategoriesRules } from '@/requests/categories-v2/get-categories-rules'
import PutDeviceIP from '@/requests/devices/put-device-ip'
import PutDeviceIp6 from '@/requests/devices-v2/put-device-ip6'
import { MetaResp } from '@/requests/request'
import PatchCategoryBlockingBaseRuleset, { CategoriesRuleset } from '@/requests/blocking/patch-category-blocking-base-ruleset'
import PatchAppBlockingBaseRuleset from '@/requests/blocking/patch-app-blocking-base-ruleset'
import { Profile } from '@/requests/knox-api/put-device-profile'
import GetAppBlocking, { AppModel } from '@/requests/blocking/get-app-blocking'
import PatchCategoriesRules from '@/requests/categories-v2/patch-categories-rules'
import cloneDeep from 'lodash/cloneDeep'
import GetCategoryBlocking from '@/requests/blocking/get-category-blocking'

export interface UnblockingArgs {
  unblockPlayStore?: boolean
  unblockGalaxyStore?: boolean
  unblockFacebook?: boolean
  unblockChrome?: boolean
  unblockSBrowser?: boolean
  unblockInstagram?: boolean
  unblockReddit?: boolean
  unblockSnapchat?: boolean
  unblockTiktok?: boolean
  youtubeSetting?: 'safe_vision' | number | null; // Null means block both
}

export function applyKspProfileSettings(profile: Profile, deviceId: string, args: UnblockingArgs | undefined): Promise<any> {
  if (profile.startsWith('basic')) {
    return applyKspBasicSettingsWithAuth('', deviceId)
  } else if (profile.startsWith('intermediate')) {
    return applyKspIntermediateSettings(deviceId, args as UnblockingArgs)
  } else {
    return applyKspAdvancedSettings(deviceId, args as UnblockingArgs)
  }
}

function defaultAppAllowlist(): {[app: string]: AppModel | null} {
  return {
    'android/com.sec.android.app.myfiles': {
      blocked: false
    },
    'android/com.samsung.android.messaging': {
      blocked: false
    },
    'android/com.samsung.android.dialer': {
      blocked: false
    },
    'android/com.sec.android.app.camera': {
      blocked: false
    },
    'android/com.sec.android.gallery3d': {
      blocked: false
    },
    'android/com.sec.android.app.clockpackage': {
      blocked: false
    },
    'android/com.samsung.android.app.contacts': {
      blocked: false
    },
    'android/com.samsung.android.calendar': {
      blocked: false
    },
    'android/com.sec.android.app.popupcalculator': {
      blocked: false
    },
    'android/com.samsung.android.app.notes': {
      blocked: false
    },
    'android/com.samsung.android.app.watchmanager': {
      blocked: false
    },
    'android/com.sec.android.app.fm': {
      blocked: false
    },
    'android/com.google.android.apps.messaging': {
      blocked: false
    }
  }
}

export function applyKspBasicSettingsWithAuth(auth: string, deviceId: string): Promise<any> {
  // Allow some initial apps and make others require approval.
  // This filters out some apps (many of which are region-specific and hard to find)
  // that we don't want available.
  // In case we are switching *back* to basic, only allow the default apps again.
  return Promise.all([
    new Promise<void>((resolve, reject) => {
      const getAppsReq = new GetAppBlocking()
      if (auth) {
        getAppsReq.setAuth(auth)
      }
      getAppsReq.handle({
        'device-id': deviceId
      }).then(apps => {
        const currentAppsToDelete: {[app: string]: AppModel | null} = {}
        if (apps.spec && apps.spec.meta && apps.spec.meta.appOrder) {
          apps.spec.meta.appOrder.forEach(app => {
            currentAppsToDelete[app] = null
          })
        }
        const appsReq = new PatchAppBlockingBaseRuleset()
        if (auth) {
          appsReq.setAuth(auth)
        }
        appsReq.handle({
          deviceId: deviceId,
          ruleset: {
            approvalRequired: true,
            apps: assign(currentAppsToDelete, defaultAppAllowlist())
          }
        }).then(() => resolve()).catch(err => reject(err))
      }).catch(err => reject(err))
    }),
    applyProfileWithAuth(auth, deviceId, '', PROFILE_KSP_BASIC)
  ])
}

function applyKspIntermediateSettings(deviceId: string, args: UnblockingArgs): Promise<any> {
  const apps: {[app: string]: AppModel | null} = {}
  apps['android/vision.safe.kids'] = {
    blocked: args.youtubeSetting !== 'safe_vision'
  }
  const youtubeAppsBlocked = args.youtubeSetting === 3 || args.youtubeSetting === null || args.youtubeSetting === 'safe_vision'
  apps['android/com.google.android.apps.youtube.music'] = {
    blocked: youtubeAppsBlocked
  }
  apps['android/com.google.android.apps.youtube.kids'] = {
    blocked: youtubeAppsBlocked
  }
  apps['android/com.google.android.youtube'] = {
    blocked: youtubeAppsBlocked
  }
  if (args.unblockPlayStore) {
    apps['android/com.android.vending'] = {
      blocked: false
    }
  }
  if (args.unblockGalaxyStore) {
    apps['android/com.sec.android.app.samsungapps'] = {
      blocked: false
    }
  }
  if (args.unblockSBrowser) {
    apps['android/com.sec.android.app.sbrowser'] = {
      blocked: false
    }
    apps['android/com.android.chrome'] = {
      blocked: true
    }
  }
  if (args.unblockChrome) {
    apps['android/com.android.chrome'] = {
      blocked: false
    }
  }
  const applyAppsAndCategories = (toDelete: string[]): Promise<any> => {
    const deleteApps: {[app: string]: AppModel | null} = {}
    toDelete.forEach(app => {
      deleteApps[app] = null
    })
    const profile = cloneDeep(PROFILE_KSP_INTERMEDIATE)
    if (args.youtubeSetting !== null && args.youtubeSetting !== undefined) {
      profile.restrictionSettings[0].action = args.youtubeSetting === 'safe_vision' ? 0 : args.youtubeSetting
    }
    return Promise.all([
      new PatchAppBlockingBaseRuleset().handle({
        deviceId: deviceId,
        ruleset: {
          approvalRequired: true,
          apps: assign(deleteApps, defaultAppAllowlist(), apps)
        }
      }),
      applyProfile(deviceId, '', profile)
    ])
  }
  return new Promise((resolve, reject) => {
    const youtubeSetting = args.youtubeSetting
    if (youtubeSetting === undefined) {
      applyAppsAndCategories([]).then(it => resolve(it)).catch(it => reject(it))
    } else {
      // TODO: use a better API
      new GetAppBlocking().handle({
        'device-id': deviceId,
        resolve: true
      }).then(appBlocking => {
        const toDelete = appBlocking.spec.resolvedBlockingModel ? Object.keys(appBlocking.spec.resolvedBlockingModel) : []
        new GetCategoriesRules().handle({
          'device-id': deviceId,
          'mac': ''
        }).then(rules => {
          rules.spec.hasRules = true
          if (!rules.spec.blacklist) {
            rules.spec.blacklist = []
          }
          rules.spec.blacklist = rules.spec.blacklist.filter(domain => domain !== 'www.youtube-nocookie.com' && domain !== 'm.youtube.com' && domain !== 'www.youtube.com')
          if (youtubeSetting === 'safe_vision' || youtubeSetting === null) {
            // Block YouTube using domains
            rules.spec.blacklist.push('www.youtube-nocookie.com', 'm.youtube.com', 'www.youtube.com')
          } else {
            // Apply selected setting and remove domains above
            rules.spec.restrictions.filter(it => it.id === 7)[0].action = youtubeSetting
          }
          new PatchCategoriesRules().handle({
            'device-id': deviceId,
            'mac': '',
            rules: rules.spec
          }).then(() => {
            applyAppsAndCategories(toDelete).then(it => resolve(it)).catch(it => reject(it))
          }).catch(it => reject(it))
        }).catch(it => reject(it))
      }).catch(it => reject(it))
    }
  })
}

function applyKspAdvancedSettings(
  deviceId: string, args: UnblockingArgs,
): Promise<any> {
  const apps: {[app: string]: AppModel | null} = {}
  // Always block reddit app
  apps['android/com.reddit.frontpage'] = {
    blocked: true
  }
  if (args.unblockFacebook) {
    apps['android/com.facebook.lite'] = {
      blocked: false
    }
    apps['android/com.facebook.katana'] = {
      blocked: false
    }
    apps['android/com.facebook.mlite'] = {
      blocked: false
    }
    apps['android/com.facebook.talk'] = {
      blocked: false
    }
    apps['android/com.facebook.orca'] = {
      blocked: false
    }
  }
  if (args.unblockInstagram) {
    apps['android/com.instagram.layout'] = {
      blocked: false
    }
    apps['android/com.instagram.lite'] = {
      blocked: false
    }
    apps['android/com.instagram.barcelona'] = {
      blocked: false
    }
    apps['android/com.instagram.android'] = {
      blocked: false
    }
  }
  if (args.unblockSnapchat) {
    apps['android/com.snapchat.android'] = {
      blocked: false
    }
  }
  if (args.unblockTiktok) {
    apps['android/co.triller.droid'] = {
      blocked: false
    }
    apps['android/com.mx.takatak.video.downloader'] = {
      blocked: false
    }
    apps['android/in.mohalla.video'] = {
      blocked: false
    }
    apps['android/com.tigo.flower'] = {
      blocked: false
    }
    apps['android/com.ss.android.ugc.boom'] = {
      blocked: false
    }
    apps['android/com.lemon.faceu.oversea'] = {
      blocked: false
    }
    apps['android/com.gorgeous.liteinternational'] = {
      blocked: false
    }
    apps['android/com.xt.retouchoversea'] = {
      blocked: false
    }
    apps['android/com.tiktok.tv'] = {
      blocked: false
    }
    apps['android/com.zhiliao.musically.livewallpaper'] = {
      blocked: false
    }
    apps['android/com.tiktokshop.seller'] = {
      blocked: false
    }
    apps['android/com.zhiliaoapp.musically.go'] = {
      blocked: false
    }
    apps['android/com.zhiliaoapp.musically'] = {
      blocked: false
    }
  }
  const youtubeAppsBlocked = args.youtubeSetting === 3 || args.youtubeSetting === null || args.youtubeSetting === 'safe_vision'
  apps['android/com.google.android.apps.youtube.music'] = {
    blocked: youtubeAppsBlocked
  }
  apps['android/com.google.android.apps.youtube.kids'] = {
    blocked: youtubeAppsBlocked
  }
  apps['android/com.google.android.youtube'] = {
    blocked: youtubeAppsBlocked
  }
  const applyApps = (): Promise<any> => {
    return new PatchAppBlockingBaseRuleset().handle({
      deviceId: deviceId,
      ruleset: {
        approvalRequired: false,
        apps: apps
      }
    })
  }
  return new Promise((resolve, reject) => {
    new PatchAppBlockingBaseRuleset().handle({
      deviceId: deviceId,
      ruleset: {
        approvalRequired: false,
        apps: {}
      }
    }).then(() => {
      applyProfile(deviceId, '', PROFILE_KSP_INTERMEDIATE).then(() => {
        const youtubeSetting = args.youtubeSetting
        if (youtubeSetting === undefined) {
          applyApps().then(it => resolve(it)).catch(it => reject(it))
        } else {
          // TODO: use a better API
          new GetCategoriesRules().handle({
            'device-id': deviceId,
            'mac': ''
          }).then(rules => {
            rules.spec.hasRules = true
            if (!rules.spec.blacklist) {
              rules.spec.blacklist = []
            }
            rules.spec.blacklist = rules.spec.blacklist.filter(domain => domain !== 'www.youtube-nocookie.com' && domain !== 'm.youtube.com' && domain !== 'www.youtube.com')
            if (youtubeSetting === 'safe_vision' || youtubeSetting === null) {
              // Block YouTube using domains
              rules.spec.blacklist.push('www.youtube-nocookie.com', 'm.youtube.com', 'www.youtube.com')
            } else {
              // Apply selected setting and remove domains above
              rules.spec.restrictions.filter(it => it.id === 7)[0].action = youtubeSetting
            }
            if (args.unblockTiktok) {
              rules.spec.categories.filter(it => it.id === 131)[0].action = 1
            }
            if (args.unblockSnapchat) {
              rules.spec.categories.filter(it => it.id === 127)[0].action = 1
            }
            if (args.unblockReddit) {
              rules.spec.categories.filter(it => it.id === 178)[0].action = 1
            }
            if (args.unblockInstagram) {
              rules.spec.categories.filter(it => it.id === 130)[0].action = 1
            }
            if (args.unblockFacebook) {
              rules.spec.categories.filter(it => it.id === 126)[0].action = 1
              rules.spec.categories.filter(it => it.id === 438)[0].action = 1
            }
            new PatchCategoriesRules().handle({
              'device-id': deviceId,
              'mac': '',
              rules: rules.spec
            }).then(() => {
              applyApps().then(it => resolve(it)).catch(it => reject(it))
            }).catch(it => reject(it))
          }).catch(it => reject(it))
        }
      })
    }).catch(it => reject(it))
  })
}

function applyProfileToRuleset(profile: BlockingProfile, ruleset: CategoriesRuleset) {
  profile.include.forEach(inclusion => {
    applyProfileToRuleset(inclusion, ruleset)
  })
  profile.blockedCategories.forEach(category => {
    ruleset.categories[category] = {
      blocked: true
    }
  })
  if (profile.unblockedCategories) {
    profile.unblockedCategories.forEach(category => {
      ruleset.categories[category] = {
        blocked: false
      }
    })
  }
  profile.restrictionSettings.forEach(setting => {
    ruleset.categories[setting.id] = {
      selectedOption: setting.action
    }
  })
  if (profile.remove) {
    profile.remove.forEach(toRemove => delete ruleset.categories[toRemove])
  }
}

export function applyProfile(deviceId: string, mac: string, profile: BlockingProfile): Promise<MetaResp<null>> {
  return applyProfileWithAuth('', deviceId, mac, profile)
}

export function applyProfileWithAuth(auth: string, deviceId: string, mac: string, profile: BlockingProfile): Promise<MetaResp<null>> {
  const ruleset: CategoriesRuleset = {
    categories: {}
  }
  const req = new PatchCategoryBlockingBaseRuleset()
  if (auth) {
    req.setAuth(auth)
  }
  applyProfileToRuleset(profile, ruleset)
  return req.handle({
    'device-id': deviceId,
    mac: mac,
    ruleset: ruleset
  })
}

/**
 * @returns The IANA timezone string of the user, falling back to UTC.
 */
export function getTimezone (): string {
  let tzstring = 'UTC'
  if (window.Intl) {
    tzstring = Intl.DateTimeFormat().resolvedOptions().timeZone
  }
  return tzstring
}

/**
 * @returns A title case string, e.g. Title Case.
 * @param input String to convert.
 */
export function titleCase (input: string): string {
  return startCase(toLower(input))
}

export interface JWTClaims {
  id: string;
  deviceID: string;
  subStatus: string;
  nextBill: number;
  purchaseToken: string;
  planID: string;
  roles: string[];
  requestedExpiry: number;
  requestedDeviceID: string;
  requestedRoles: string[];
  exp: number;
}

/**
 * @returns The JWT claims object, given a JWT.
 * @param jwt The JWT string.
 */
export function parseClaims (jwt: string): JWTClaims {
  const parts = jwt.split('.')
  if (parts.length !== 3) {
    throw new Error('Invalid JWT')
  }
  const claimsPart = atob(parts[1])
  return JSON.parse(claimsPart)
}

/**
 * @returns Whether the web app has a valid auth token.
 */
export function hasValidAuth (): boolean {
  const jwt = localStorage.getItem(TOKEN_STORAGE_NAME)
  if (!jwt) {
    return false // No token stored
  }
  // Parse claims
  const claims = parseClaims(jwt)
  if (typeof claims.exp === 'number') {
    // Have valid expiry
    const timestampNow = Math.round(new Date().getTime() / 1000)
    return timestampNow < claims.exp // Whether is expired
  }
  return false // No valid expiry
}

/**
 * @param redirectFullPath The full path of the page to back to once logged in.
 * @returns A location that will go to the login page. 
 */
export function getLoginRoute (currRoute: Route, redirectFullPath: string): RawLocation {
  const query: {[key: string]: string | (string | null)[]} = {
    redirect: redirectFullPath,
    reason: 'You must log in to view this page.'
  }
  if (currRoute.query['ref']) {
    query['ref-follow'] = currRoute.query['ref']
  }
  if (currRoute.query['ref-follow']) {
    query['ref-follow'] = currRoute.query['ref-follow']
  }
  return {
    name: 'Login',
    query: query
  }
}

export function getTokenPrimaryRole (token: string): string | undefined {
  const claims = parseClaims(token)
  if (!claims.roles) {
    return undefined
  }
  if (!claims.roles.length) {
    return undefined
  }
  return claims.roles[0]
}

export function needsTwofactor (token: string): boolean {
  return getTokenPrimaryRole(token) === 'complete-twofactor'
}

/**
 * From https://webpushdemo.azurewebsites.net/
 * @returns A Uint8Array of a base64-encoded URL.
 * @param base64String The base64-encoded URL.
 */
export function urlBase64ToUint8Array(base64String: string) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
      .replace(/-/g, '+')
      .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i)  {
      outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

/**
 * @returns Whether we are in the development environment.
 */
export function isDevEnvironment(): boolean {
  return process.env.NODE_ENV === 'development' || window.location.host === 'staging.my.safesurfer.io'
}

/**
 * @returns The content of the meta tag with the name, or the empty string if it wasn't defined.
 * @param tagName The name of the meta tag.
 */
export function loadFromMetaTag(tagName: string): string {
  const el = document.head.querySelector('[name~=' + tagName + '][content]') as HTMLMetaElement
  if (!el) {
    return ''
  }
  return el.content
}

/**
 * @returns The content of a meta tag with the name, or a default value if the meta tag wasn't found or isn't set.
 * @param tagName The name of the meta tag.
 * @param defaultValue The value to use if the meta tag wasn't found. 
 * */ 
export function metaTagOrDefault(tagName: string, defaultValue: string): string {
  const content = loadFromMetaTag(tagName)
  if (!content || content.startsWith('{{')) {
    return defaultValue
  }
  return content
}

/**
 * @returns The base URL to use for the API.
 */
export function getBaseURL(): string {
  if (isDevEnvironment()) {
    return BASE_URL_STAGING
  }
  return loadFromMetaTag('apiURL')
}

/**
 * @returns The base URL to use for the IPV6-only API.
 */
export function getBaseURLIpv6(): string {
  if (isDevEnvironment()) {
    return BASE_URL_STAGING_IPV6
  }
  return loadFromMetaTag('apiIpv6URL')
}

/**
 * @returns The host used for DOT in this deployment.
 */
export function getDotHost(): string {
  return metaTagOrDefault('dotHost', 'dns.safesurfer.io')
}

/**
 * @returns The DOH URL for this deployment.
 */
export function getDohURL(): string {
  return metaTagOrDefault('dohURL', 'https://doh.safesurfer.io/')
}

export function getChargebeeDefaultPlan(): string {
  return metaTagOrDefault('chargebeeDefaultPlan', 'pro-2021-annual')
}

/**
 * @returns The chargebee site to use for payment.
 */
export function getChargebeeSite(): string {
  if (isDevEnvironment()) {
    return CHARGEBEE_SITE_STAGING
  }
  return metaTagOrDefault('chargebeeSite', 'safesurfer')
}

/**
 * @returns The URL to the categorizer.
 */
export function getCategorizerURL(): string {
  return metaTagOrDefault('categorizerURL', 'https://categorizer.safesurfer.io')
}

/**
 * Log something to the console, only if we are in the dev environment.
 * @param msg 
 */
export function log(msg: any): void {
  if (isDevEnvironment()) {
    console.log(msg)
  }
}

/**
 * @returns A simple string description of the user's subscription status.
 * @param subStatus The subscription status from the JWT.
 */
export function getSubscriptionDescription(subStatus: string, isPromo?: boolean): string {
  if (isPromo) {
    return 'Promo'
  }
  if (subStatus === 'ACTIVE' || subStatus === 'COURTESY' || subStatus === 'NON_RENEWING' || subStatus === 'FUTURE') {
    return 'Pro Surfer'
  } else if (subStatus === 'TRIAL' || subStatus === 'TRIAL_CANCELLED') {
    return 'Trial'
  } else {
    return 'Basic Surfer'
  }
}

/**
 * @returns The name of the role that routers will try to authenticate as,
 * since we must match this.
 */
export function getRouterRole(): string {
  return metaTagOrDefault('routerRole', 'router')
}

/**
 * @returns The page to open to show the router UI. 
 */
export function getRouterHost(): string {
  return metaTagOrDefault('routerHost', 'http://mydevice.safesurfer.co.nz')
}

export function getPlainProtoDomain(): string {
  return metaTagOrDefault('plainProtoDomain', 'plain.proto.safesurfer.io')
}

export function getDohProtoDomain(): string {
  return metaTagOrDefault('dohProtoDomain', 'doh.proto.safesurfer.io')
}

export function getDotProtoDomain(): string {
  return metaTagOrDefault('dotProtoDomain', 'dot.proto.safesurfer.io')
}

export function getDnscryptProtoDomain(): string {
  return metaTagOrDefault('dnscryptProtoDomain', 'dnscrypt.proto.safesurfer.io')
}

export function getRouterProtoDomain(): string {
  return metaTagOrDefault('routerProtoDomain', 'router.proto.safesurfer.io')
}

export class IpResults {
  public ip4: LinkSourceIpResult | undefined
  public ip6: LinkSourceIpResult | undefined

  ipv6Available(): Boolean {
    return this.ip6 === LinkSourceIpResult.Ok
  }
}

export enum LinkSourceIpResult {
  Ok, Conflict, Unknown, NotRelevant
}

export function linkSourceIp(device: string): Promise<IpResults> {
  const result = new IpResults()
  return new Promise((resolve, reject) => {
    new PutDeviceIP().handle(device).then(() => {
      result.ip4 = LinkSourceIpResult.Ok
    }).catch(err => {
      if (err) {
        if (err == 400) {
          result.ip4 = LinkSourceIpResult.Conflict
        } else {
          result.ip4 = LinkSourceIpResult.Unknown
        }
      } else {
        result.ip4 = LinkSourceIpResult.Unknown
      }
    }).then(() => {
      new PutDeviceIp6().handle(device).then(() => {
        result.ip6 = LinkSourceIpResult.Ok
      }).catch(err => {
        if (err) {
          if (err == 400) {
            result.ip6 = LinkSourceIpResult.Conflict
          } else {
            result.ip6 = LinkSourceIpResult.Unknown
          }
        } else {
          result.ip6 = LinkSourceIpResult.NotRelevant
        }
      }).then(() => resolve(result))
    })
  })
}

/**
 * @returns Whether the return URL for the router is allowed. This URL will receive
 * the router's HMAC key and the user's email, so it's important that we don't
 * leak to unauthorized domains.
 * @param ret The proposed return url.
 */
export function routerReturnURLAllowed(ret: string): boolean {
  const allowedPrefixes: string[] = isDevEnvironment() ?
    [ 'http://192.168.8.1', 'http://localhost:', 'http://mydevice.safesurfer.co.nz', 'http://homenetwork.safesurfer.io' ] :
    JSON.parse(metaTagOrDefault('routerHosts', '[]'))
  for (let i = 0; i < allowedPrefixes.length; i++) {
    if (ret.startsWith(allowedPrefixes[i])) {
      return true
    }
  }
  return false
}

export function getCustomRulesSummary (rules: CategoriesRules): string {
  const blocked: string[] = []
  const unblocked: string[] = []
  const changes: string[] = []
  rules.categories.forEach(category => {
    if (category.action === 0) {
      blocked.push(category.displayName)
    } else if (category.action === 1) {
      unblocked.push(category.displayName)
    }
  })
  rules.apps.forEach(app => {
    if (app.blocked) {
      blocked.push(app.name + ' App')
    } else {
      unblocked.push(app.name + ' App')
    }
  })
  // Find added blacklist items
  rules.blacklist.forEach(domain => {
    blocked.push(domain)
  })
  // Find added whitelist items
  rules.whitelist.forEach(domain => {
    unblocked.push(domain)
  })
  // Find restriction changes
  rules.restrictions.forEach(restriction => {
    changes.push(restriction.displayName + ' (' + restriction.actions[restriction.action] + ')')
  })
  // If no rules changes, just say that
  if (blocked.length === 0 && unblocked.length === 0 && changes.length === 0) {
    return 'No effect, no sites added'
  }
  let sentence = ''
  if (blocked.length > 0) {
    sentence += 'Block '
    blocked.forEach((category, i) => {
      sentence += category
      if (i < blocked.length - 2) {
        sentence += ', '
      } else if (i === blocked.length - 2) {
        sentence += ' and '
      }
    })
  }
  if (unblocked.length > 0) {
    if (blocked.length > 0) {
      sentence += '; '
    }
    sentence += 'Allow '
    unblocked.forEach((category, i) => {
      sentence += category
      if (i < unblocked.length - 2) {
        sentence += ', '
      } else if (i === unblocked.length - 2) {
        sentence += ' and '
      }
    })
  }
  if (changes.length > 0) {
    if (blocked.length > 0 || unblocked.length > 0) {
      sentence += '; '
    }
    sentence += 'Update restriction for '
    changes.forEach((category, i) => {
      sentence += category
      if (i < changes.length - 2) {
        sentence += ', '
      } else if (i === changes.length - 2) {
        sentence += ' and '
      }
    })
  }
  return sentence
}

export function getTimeBasedRulesSummary(rules: TimeBasedRules, timesOnly: boolean, effectOnly: boolean): string {
  let sentence = ''
  if (!timesOnly) {
    sentence += getCustomRulesSummary(rules.customRules)
  }
  if (Object.keys(rules.activeTimes).every(day => rules.activeTimes[day].every(active => !active))) {
    return sentence + '. No effect, is never active.'
  }
  if (effectOnly) {
    return sentence
  }
  sentence += ' from '
  // Summarize the active times
  Object.keys(rules.activeTimes).forEach(day => {
    const activeTimes = rules.activeTimes[day]
    const activePortionStrings: string[][] = []
    let inActive = false
    let lastActiveStart = 0
    for (let i = 0; i <= 96; i++) { // The off-by-one here is intentional so we don't miss final interval
      if (activeTimes[i]) {
        inActive = true
        if (!activeTimes[i-1]) {
          lastActiveStart = i
        }
      } else {
        if (inActive) {
          const startHour = Math.floor(lastActiveStart / 4)
          const startQuarter = lastActiveStart % 4
          const endHour = Math.floor((i-1) / 4)
          const endQuarter = (i-1) % 4
          activePortionStrings.push([formatHourAndQuarter(startHour, startQuarter, true), formatHourAndQuarter(endHour, endQuarter, false)])
        }
        lastActiveStart = -1
        inActive = false
      }
    }
    if (activePortionStrings.length === 0) {
      return
    }
    // There can be a small issue where if we cross the "12am" interval we will end up with two separate intervals.
    // Stitch them together if this is applicable.
    let starts12AMIndex = -1
    let ends12AMIndex = -1
    activePortionStrings.forEach((interval, i) => {
      if (interval[0] === '12am') {
        starts12AMIndex = i
      } else if (interval[1] === '12am') {
        ends12AMIndex = i
      }
    })
    if (starts12AMIndex !== -1 && ends12AMIndex !== -1) {
      const stitched = [activePortionStrings[ends12AMIndex][0], activePortionStrings[starts12AMIndex][1]]
      activePortionStrings.splice(ends12AMIndex, 1, stitched)
      activePortionStrings.splice(starts12AMIndex, 1)
    }
    // Format the sentence
    activePortionStrings.forEach((interval, i) => {
      sentence += interval[0] + ' to ' + interval[1]
      if (i < activePortionStrings.length - 2) {
        sentence += ', '
      } else if (i === activePortionStrings.length - 2) {
        sentence += ' and '
      }
    })
    sentence += ' '
    switch (day) {
      case '*':       sentence += ' every day';     break;
      case 'weekend': sentence += ' on weekends';   break;
      case 'weekday': sentence += ' on weekdays';   break;
      case '2':       sentence += ' on Mondays';    break;
      case '3':       sentence += ' on Tuesdays';   break;
      case '4':       sentence += ' on Wednesdays'; break; 
      case '5':       sentence += ' on Thursdays';  break;
      case '6':       sentence += ' on Fridays';    break;
      case '0':       sentence += ' on Saturdays';  break;
      case '1':       sentence += ' on Sundays';    break;
    }
    sentence += '; '
  })
  if (sentence.endsWith('; ')) {
    sentence = sentence.substring(0, sentence.length-2)
  }
  sentence += '.'
  return sentence
}

/**
  * @returns A formatted time given an hour and quarter.
  */
export function formatHourAndQuarter (hour: number, quarter: number, isStart: boolean): string {
  let middlePortion = ''
  const minutes = 15*(quarter + (isStart ? 0 : 1))
  if (minutes === 60) {
    hour += 1
  } else if (minutes !== 0) {
    middlePortion += ':' + minutes
  }
  if (hour === 0) {
    return 12 + middlePortion + 'am'
  } else if (hour < 12) {
    return hour + middlePortion + 'am'
  } else if (hour === 12) {
    return 12 + middlePortion + 'pm'
  } else if (hour === 24) {
    return 12 + middlePortion + 'am'
  } else {
    return (hour - 12) + middlePortion + 'pm'
  }
}

/**
 * @param device The device to check.
 * @returns Whether screentime features are supported for a device. 
 */
export function screentimeSupported (device: APIDevice): boolean {
  return !(device.type === 'op-device' && (!device.version || parseInt(device.version.split('.')[0]) < 4))
}

export interface DeviceShortcut {
  name: string;
  route: Location;
}

/**
 * @returns A list of device shortcuts for the device and subscription status.
 * These are used on the devices/device pages.
 */
export function getDeviceShortcuts (device: APIDevice, isPro: boolean | undefined, isDevicePage: boolean): DeviceShortcut[] {
  const shortcuts: DeviceShortcut[] = [
    {
      name: 'Blocking',
      route: {
        name: 'BlockCategories',
        query: {
          'device-id': device.id,
          mac: device.mac
        }
      }
    }
  ]
  if (isPro) {
    shortcuts.push({
      name: 'Browsing history',
      route: {
        name: 'Usage',
        query: {
         'device-id': device.id,
         'mac': device.mac,
          section: 'blocked'
        }
      }
    })
    if (device.version) {
      const versionSplit = device.version.split('.')
      if (versionSplit.length > 1) {
        const major = parseInt(versionSplit[0])
        const minor = parseInt(versionSplit[1])
        if (major > 3 || (major === 3 && minor >= 5)) {
          shortcuts.push({
            name: 'App approval',
            route: {
              name: 'AppApproval',
              query: {
                'device-id': device.id
              }
            }
          })
        }
      }
    }
    if (device.metadata && device.metadata['knox-serial'] !== undefined) {
      shortcuts.push({
        name: 'Location',
        route: {
          name: 'Location',
          query: {
            'device-id': device.id
          }
        }
      })
      if (!isDevicePage) {
        shortcuts.push({
          name: 'Switch Profile',
          route: {
            name: 'Device',
            query: {
              'device-id': device.id
            }
          }
        })
      }
    }
    // shortcuts.push({
    //   name: 'Security Alerts',
    //   route: {
    //     name: 'SecurityAlerts',
    //     query: {
    //       'device-id': device.id,
    //       'mac': device.mac,
    //     }
    //   }
    // })
  } else {
    return shortcuts
  }
  switch (device.type) {
    case 'android-device':
      shortcuts.push({
        name: 'Nudity detection',
        route: {
          name: 'Screencasts',
          query: {
            device: device.id
          }
        }
      })
      shortcuts.push({
        name: 'Texts & social media',
        route: {
          name: 'NotificationLog',
          query: {
            'device-id': device.id
          }
        }
      })
      break
    case "op-device":
      if (device.mac !== '') {
        shortcuts.push({
          name: 'VPN Attempts',
          route: {
            name: 'VPNAlerts',
            query: {
              id: device.id,
              mac: device.mac
            }
          }
        })
      }
      break
  }
  if (screentimeSupported(device)) {
    shortcuts.push({
      name: 'Pause internet',
      route: {
        name: 'StopStartAccess',
        query: {
          'device-id': device.id,
          'mac': device.mac
        }
      }
    })
    shortcuts.push({
      name: 'Timers',
      route: {
        name: 'ScreentimeTimers',
        query: {
          'device-id': device.id,
          'mac': device.mac
        }
      }
    })
    shortcuts.push({
      name: 'Timetables',
      route: {
        name: 'ScreentimeTimetables',
        query: {
          'device-id': device.id,
          'mac': device.mac
        }
      }
    })
  }
  return shortcuts
}

export function getKnoxApiUrl(): string {
  if (isDevEnvironment()) {
    return KNOX_API_URL_STAGING
  }
  return KNOX_API_URL
}

const doh = require('dohjs')

export interface PlainDnsServers {
  v4: string[]
  v6: string[]
}

// Use a combination of Cloudflare DNS and traffic manager to roughly determine the
// best-performing GCP servers according to the traffic manager config.
// The actual IPs returned by the lookup are not relevant, although they do work,
// the global one is used for Azure in all cases anyway.
export function getBestPlainDnsServers(): Promise<PlainDnsServers> {
    const resolver = new doh.DohResolver('https://1.1.1.1/dns-query') as any
    return new Promise((resolve, reject) => {
        resolver.query('ss-plain-dns.trafficmanager.net', 'A').then((response: any) => {
            if (response.answers) {
                var results = response.answers.map((it: any) => it.data)
                if (results.includes('172.190.252.119')) {
                    // US
                    resolve({
                        'v4': shuffle(['104.197.28.121', '4.150.168.57']),
                        'v6': shuffle(['2600:1900:4000:524c:8000:1::', '2603:1030:7:7::3b'])
                    })    
                    return
                } else if (results.includes('4.200.56.132')) {
                    // AU
                    resolve({
                        'v4': shuffle(['34.116.72.241', '4.150.168.57']),
                        'v6': shuffle(['2600:1900:40b0:aa3e:8000::', '2603:1030:7:7::3b'])
                    })
                    return
                } else if (results.includes('4.144.61.48')) {
                    // Asia
                    resolve({
                        'v4': shuffle(['104.155.237.225', '4.150.168.57']),
                        'v6': shuffle(['2600:1900:4030:6b9d:8000::', '2603:1030:7:7::3b'])
                    })
                    return
                }
            }
            // Something is wrong... return the old defaults
            resolve({
                'v4': shuffle(['104.197.28.121', '104.155.237.225']),
                'v6': shuffle(['2600:1900:4000:524c:8000:1::', '2600:1900:4030:6b9d:8000::'])
            })
        }).catch((e: any) => {
          console.log('couldn\'t resolve plain DNS servers', e)
          resolve({
            'v4': shuffle(['104.197.28.121', '104.155.237.225']),
            'v6': shuffle(['2600:1900:4000:524c:8000:1::', '2600:1900:4030:6b9d:8000::'])
          })
        })
    })
}

export function getBlockedBaseCategories(deviceId: string): Promise<number[]> {
  return new Promise<number[]>((resolve, reject) =>
    new GetCategoryBlocking().handle({
      resolve: true,
      'device-id': deviceId,
      'exclude-screentime-rules': true
    }).then(categories => {
      if (!categories.spec || !categories.spec.resolvedBlockingModel) {
        resolve([])
        return
      }
      const blocked: number[] = []
      Object.keys(categories.spec.resolvedBlockingModel).forEach(category => {
        const categoryId = parseInt(category)
        const resolved = categories.spec.resolvedBlockingModel[categoryId]
        if (resolved.blocked) {
          blocked.push(categoryId)
        }
      })
      resolve(blocked)
    }).catch(err => reject(err))
  )
}
