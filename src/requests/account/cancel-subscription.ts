import Request from '@/requests/request'

export default class CancelSubscription extends Request<undefined, undefined> {
  /**
   * Cancel the user's pro subscription.
   */
  handle(): Promise<undefined> {
    return this.authReq({
      url: '/user/subscription',
      method: 'DELETE'
    })
  }
}
