import Request, { MetaResp } from '@/requests/request'

export default class DeleteAccount extends Request<MetaResp<null>, string> {
  /**
   * Delete the signed-in user's account.
   * @param opts Tne user's email.
   */
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user',
      method: 'DELETE',
      params: {
        email: opts
      }
    })
  }
}
