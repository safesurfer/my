import Request from '@/requests/request'

export default class GetAccountIP extends Request<string, undefined> {
  /**
   * @returns The on-record IP for the currently signed in account.
   */
  handle (): Promise<string> {
    return this.authReq({
      url: '/user/ip'
    })
  }
}
