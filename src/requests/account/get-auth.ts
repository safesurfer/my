import Request from '@/requests/request'

export interface UserSignin {
  /**
   * The user's email.
   */
  email: string;
  /**
   * The user's password.
   */
  password: string;
}

/**
 * The response from the API.
 */
export interface APIAuthResponse {
  token: string;
  emailOtpId: string;
}

export default class GetAuth extends Request<APIAuthResponse, UserSignin> {
  handle (opts: UserSignin): Promise<APIAuthResponse> {
    return this.simpleReq({
      url: '/user/auth',
      method: 'POST',
      data: this.encodeAsForm({
        email: opts.email,
        password: opts.password,
      })
    })
  }
}
