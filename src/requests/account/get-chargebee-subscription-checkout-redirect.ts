import Request from '@/requests/request'

export default class GetChargebeeSubscriptionCheckoutRedirect extends Request<any, undefined> {
  /**
   * @returns The hosted page object for the pro checkout.
   */
  handle(): Promise<any> {
    return this.authReq({
      url: '/user/subscription/chargebee/redirect/checkout',
      method: 'GET'
    })
  }
}
