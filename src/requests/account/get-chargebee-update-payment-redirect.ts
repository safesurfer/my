import Request from '@/requests/request'

export default class GetChargebeeUpdatePaymentRedirect extends Request<any, undefined> {
  /**
   * @returns The hosted page object for the chargebee payment methods.
   */
  handle(): Promise<any> {
    return this.authReq({
      url: '/user/subscription/chargebee/redirect/payment-methods',
      method: 'GET'
    })
  }
}
