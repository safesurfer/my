import Request from "../request"

export default class GetExtraDevicesAddonQuantity extends Request<number, undefined> {
  /**
   * @returns The current number of addon devices for this account.
   */
  handle(): Promise<number> {
    return this.authReq({
      url: '/user/subscription/addons/extradevices',
      method: 'GET'
    })
  }
}
