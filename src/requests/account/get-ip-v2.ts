import Request, { MetaResp } from '@/requests/request'

export default class GetIPV2 extends Request<MetaResp<string>, undefined> {
  /**
   * @returns The user's IP.
   */
  handle(): Promise<MetaResp<string>> {
    return this.simpleReq({
      url: '/v2/my-ip',
      method: 'GET'
    })
  }
}
