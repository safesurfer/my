import Request from '@/requests/request'
import GetIPV2 from '@/requests/account/get-ip-v2'

export default class GetIP extends Request<string, undefined> {
  /**
   * Get the user's public IP. Wrap the new version to avoid using ipify.
   */
  handle (): Promise<string> {
    return new Promise((resolve, reject) => {
      new GetIPV2().handle().then(obj => {
        resolve(obj.spec)
      }).catch(err => reject(err))
    })
  }
}
