import Request from '@/requests/request'
import GetIPV62 from '@/requests/account/get-ip6-v2'

export default class GetIP6 extends Request<string, undefined> {
  /**
   * Get the user's public IP. Wrap the new version to avoid using ipify.
   */
  handle (): Promise<string> {
    return new Promise((resolve, reject) => {
      new GetIPV62().handle().then(obj => {
        resolve(obj.spec)
      }).catch(err => reject(err))
    })
  }
}