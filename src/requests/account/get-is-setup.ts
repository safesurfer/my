import Request from '@/requests/request'

export default class GetIsSetup extends Request<boolean, undefined> {
  /**
   * @returns Whether the current user is setup.
   */
  handle(): Promise<boolean> {
    return this.authReq({
      url: '/user/is-setup',
      method: 'GET'
    })
  }
}
