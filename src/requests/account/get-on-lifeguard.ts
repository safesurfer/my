import Request from '@/requests/request'
import axios from 'axios'

export default class GetOnLifeguard extends Request<boolean, undefined> {
  /**
   * @returns Whether the device is on a lifeguard network.
   */
  handle (): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios({
        url: 'https://mydevice.safesurfer.co.nz/api/system/wireless/is-online'
      }).then(res => {
        console.log('<===========')
        console.log(res.data)
        resolve(res.data && res.data.metadata && res.data.metadata.response)
      }).catch(() => {
        reject()
      })
    })
  }
}
