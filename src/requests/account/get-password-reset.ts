import Request from '@/requests/request'

export default class GetPasswordReset extends Request<undefined, string> {
  /**
   * Send a password reset email to an email.
   * @param email The email to send to.
   */
  handle (email: string): Promise<undefined> {
    return this.simpleReq({
      url: '/user/password/reset',
      method: 'GET',
      params: {
        email: email
      }
    })
  }
}
