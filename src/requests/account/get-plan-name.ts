import Request from "../request"

export default class GetPlanName extends Request<string, undefined> {
  /**
   * @returns The name of the user's current plan.
   */
  handle(): Promise<string> {
    return this.authReq({
      url: '/user/subscription/name',
      method: 'GET'
    })
  }
}