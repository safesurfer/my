import Request, { MetaResp } from '../request'

export default class GetReferrer extends Request<MetaResp<string>, undefined> {
  /**
   * @returns The name of the user's referrer if any, or otherwise the empty string.
   */
  handle(): Promise<MetaResp<string>> {
    return this.authReq({
      url: '/v2/user/referred-by',
      method: 'GET'
    })
  }
}
