import Request from '@/requests/request'

export default class PostAccountIP extends Request<undefined, string> {
  /**
   * Sync the user's current IP with their account.
   * @param ip The current IP.
   */
  handle (ip: string): Promise<undefined> {
    return this.authReq({
      url: '/user/ip',
      method: 'POST',
      data: this.encodeAsForm({
        ip: ip
      })
    })
  }
}
