import Request from "../request"

export default class PutExtraDevicesAddonQuantity extends Request<null, number> {
  /**
   * Set the amount of extra devices for the user's subscription.
   * @param opts Quantity.
   */
  handle(opts: number): Promise<null> {
    return this.authReq({
      url: '/user/subscription/addons/extradevices',
      method: 'PUT',
      data: this.encodeAsForm({
        numExtraDevices: opts
      })
    })
  }
}
