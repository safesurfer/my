import Request from '@/requests/request'

export default class ReactivateSubscription extends Request<undefined, undefined> {
  /**
   * Reactivate the user's subscription.
   */
  handle(): Promise<undefined> {
    return this.authReq({
      url: '/user/subscription/reactivate',
      method: 'POST'
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
