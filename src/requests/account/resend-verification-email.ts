import Request from '@/requests/request'

export default class ResendVerificationEmail extends Request<undefined, undefined> {
  /**
   * Resend the verification email for the user's account.
   */
  handle (): Promise<undefined> {
    return this.authReq({
      url: '/user/verification/email',
      method: 'GET'
    })
  }
}
