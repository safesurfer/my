import Request from '@/requests/request'

export interface SubscribeOptions {
  ip: string; // The user's IP
  numExtraDevices?: number; // Extra device addon quantity
  plan?: string; // Which plan to subscribe to
  coupon?: string; // Coupon code to use
  resubscribe: boolean;
}

export default class Subscribe extends Request<string, SubscribeOptions> {
  /**
   * @returns A new JWT token representing having subscribed to the trial of pro.
   */
  handle(opts: SubscribeOptions): Promise<string> {
    console.log(opts)
    return this.authReq({
      url: '/user/subscription',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }
}
