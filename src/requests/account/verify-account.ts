import Request from '@/requests/request'

export default class VerifyAccount extends Request<'success' | 'error' | 'expired', string> {
  /**
   * Verify the user's account by sending the secret from the link.
   * @param secret The secret.
   */
  handle (secret: string): Promise<'success' | 'error' | 'expired'> {
    return this.simpleReq({
      url: '/user/verification',
      method: 'POST',
      data: this.encodeAsForm({
        secret: secret
      })
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response) {
      reject(err.response)
    } else {
      reject() // eslint-disable-line
    }
  }
}
