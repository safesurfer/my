import Request, { MetaResp } from "../request";
import { AlertsFilterSettings } from "./get-alerts-with-filter";

export type EmailSettingsFrequency = 'D' | 'W'

export interface EmailSettings {
  id: number;
  enabled: boolean;
  frequency: EmailSettingsFrequency;
  emailTimestamp: number;
  emailAddresses: string[];
  sendIfEmpty: boolean;
  includeSummary: boolean;
  filter: AlertsFilterSettings;
  lastSend: EmailLastSend | null;
  minPriority: number | null;
}

export interface EmailLastSend {
  timestamp: number;
  wasErr: boolean;
  wasEmpty: boolean;
}

export default class GetAlertsEmails extends Request<MetaResp<EmailSettings[]>, undefined> {
  handle(): Promise<MetaResp<EmailSettings[]>> {
    return this.authReq({
      url: '/v2/alerts/emails',
      method: 'GET'
    })
  }
}
