import Request, { MetaResp } from "../request";
import { AlertOrigin, AlertsResult, GetAlertsArgs } from "./get-alerts";

export interface AlertsFilterSettings {
  allDevices: boolean;
  deviceId: string;
  devices: AlertsIncludedDevices;
  categories: {[categoryID: number]: boolean};
  dataSources: AlertOrigin[];
}

export interface AlertsIncludedDevices {
  implicitExclude: boolean;
  includedDevices: {[compoundId: string]: boolean};
}

export interface GetAlertsWithFilterArgs {
  query: GetAlertsArgs;
  filter: AlertsFilterSettings;
}

export default class GetAlertsWithFilter extends Request<MetaResp<AlertsResult>, GetAlertsWithFilterArgs> {
  handle(opts: GetAlertsWithFilterArgs): Promise<MetaResp<AlertsResult>> {
    return this.authReq({
      url: '/v2/alerts/with-filter',
      method: 'POST',
      params: opts.query,
      data: opts.filter
    })
  }
}
