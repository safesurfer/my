import Request, { MetaResp } from "../request";

export default class PostAlertsDismissed extends Request<MetaResp<null>, string[]> {
  handle(opts: string[]): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/alerts/dismissed',
      method: 'POST',
      data: opts
    })
  }
}
