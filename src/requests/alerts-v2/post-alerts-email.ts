import Request, { MetaResp } from "../request";
import { EmailSettings } from "./get-alerts-emails";

export default class PostAlertEmail extends Request<MetaResp<number>, EmailSettings> {
  handle(opts: EmailSettings): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/alerts/emails',
      method: 'POST',
      data: opts
    })
  }
}
