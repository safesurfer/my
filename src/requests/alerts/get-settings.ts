import Request from '@/requests/request'

/**
 * Describes the user's alert settings.
 */
export interface APIAlertSettings {
  /**
   * Whether email alerts are enabled.
   */
  emailsEnabled: boolean;
  /**
   * 'D' or 'W' for daily or weekly emails.
   */
  emailFrequency: 'D' | 'W';
  /**
   * Unix timestamp with the hour and day (in UTC time) when the email will be sent.
   * If 0, should be initialized with the current time.
   */
  emailTimestamp: number;
  /**
   * List of email addresses to send to.
   */
  emailAddresses: string[];
  /**
   * Whether to include all devices in alerts.
   */
  allDevices: boolean;
  /**
   * Whether to implicitly exclude devices if they don't exist in the settings.
   */
  implicitExcludeDevices: boolean;
  /**
   * Which devices to include or not. Meaningful if allDevices is false.
   */
  devices: APIAlertSettingsDevices;
  /**
   * Which categories to include. Map from categories.id to whether to include.
   */
  categories: {[id: number]: boolean};
  /**
   * The user's timezone.
   */
  timezone: string;
}

/**
 * Which devices to include or not.
 */
export interface APIAlertSettingsDevices {
  /**
   * Lifeguard devices.
   */
  lifeguard: {[lifeguardID: string]: {[deviceMac: string]: boolean}};
  /**
   * Mobile devices.
   */
  mobile: {[deviceID: string]: boolean};
  /**
   * DOH devices.
   */
  doh: {[deviceID: string]: boolean};
  /**
   * Misc account devices, e.g. Linked IP.
   */
  account: boolean;
}
