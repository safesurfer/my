import Request, { MetaResp } from "../request";

export interface DeleteDeadlineArgs {
  'device-id': string;
  'mac': string;
}

export default class DeleteDeadline extends Request<MetaResp<null>, DeleteDeadlineArgs> {
  handle(opts: DeleteDeadlineArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/block-internet/deadline',
      method: 'DELETE',
      params: opts
    })
  }
}
