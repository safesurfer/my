import Request, { MetaResp } from "../request";

export interface DeleteTimerArgs {
  'device-id': string;
  'mac': string;
  'day': string;
}

export default class DeleteTimer extends Request<MetaResp<null>, DeleteTimerArgs> {
  handle(opts: DeleteTimerArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/block-internet/timers/' + opts.day,
      method: 'DELETE',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      }
    })
  }
}
