import Request, { MetaResp } from "../request";

export interface BlockInternetDeadline {
  deviceID: string;
  mac: string;
  expiry: number;
  allow: boolean;
}

export interface GetDeadlineSelfServiceArgs {
  'device-id': string;
  mac: string;
  token: string;
}

export default class GetDeadlineSelfService extends Request<MetaResp<BlockInternetDeadline>, GetDeadlineSelfServiceArgs> {
  handle(opts: GetDeadlineSelfServiceArgs): Promise<MetaResp<BlockInternetDeadline>> {
    return this.simpleReq({
      url: '/v2/block-internet/deadline/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        mac: opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts['token']
      }
    })
  }
}
