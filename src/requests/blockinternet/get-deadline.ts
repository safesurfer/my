import Request, { MetaResp } from "../request";
import { BlockInternetDeadline } from "./get-deadline-self-service";

export interface GetDeadlineArgs {
  'device-id': string;
  mac: string;
}

export default class GetDeadline extends Request<MetaResp<BlockInternetDeadline>, GetDeadlineArgs> {
  handle(opts: GetDeadlineArgs): Promise<MetaResp<BlockInternetDeadline>> {
    return this.authReq({
      url: '/v2/block-internet/deadline',
      method: 'GET',
      params: opts
    })
  }
}
