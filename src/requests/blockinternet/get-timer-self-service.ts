import Request, { MetaResp } from "../request";
import { BlockInternetTimer } from "./get-timer";

export interface GetBlockInternetTimerSelfServiceArgs {
  'device-id': string;
  'mac': string;
  'token': string;
  'day': string;
}

export default class GetTimerSelfService extends Request<MetaResp<BlockInternetTimer>, GetBlockInternetTimerSelfServiceArgs> {
  handle(opts: GetBlockInternetTimerSelfServiceArgs): Promise<MetaResp<BlockInternetTimer>> {
    return this.simpleReq({
      url: '/v2/block-internet/timers/' + opts.day + '/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
