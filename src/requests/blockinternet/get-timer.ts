import Request, { MetaResp } from "../request"

export interface BlockInternetTimer {
  deviceID: string;
  mac: string;
  maxTimeSecs: number;
  lastStart: number;
  secsSoFar: number;
  isRunning: boolean;
}

export interface GetTimerArgs {
  'device-id': string;
  'mac': string;
  'day': string;
}

export default class GetTimer extends Request<MetaResp<BlockInternetTimer>, GetTimerArgs> {
  /**
   * Get the timer for the day/device.
   */
  handle(opts: GetTimerArgs): Promise<MetaResp<BlockInternetTimer>> {
    return this.authReq({
      url: '/v2/block-internet/timers/' + opts.day,
      method: 'GET',
      params: opts
    })
  }
}

