import Request, { MetaResp } from "../request";
import { BlockInternetTimetable } from "./get-timetable";

export interface GetTimetableSelfServiceArgs {
  'device-id': string;
  'mac': string;
  'token': string;
}

export default class GetTimetableSelfService extends Request<MetaResp<BlockInternetTimetable>, GetTimetableSelfServiceArgs> {
  handle(opts: GetTimetableSelfServiceArgs): Promise<MetaResp<BlockInternetTimetable>> {
    return this.simpleReq({
      url: '/v2/block-internet/timetable/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
