import Request, { MetaResp } from "../request";

export interface BlockInternetTimetable {
  deviceID: string;
  mac: string;
  activeTimes: {[day: string]: boolean[]};
}

export interface GetTimetableArgs {
  'device-id': string
  'mac': string
}

export default class GetTimetable extends Request<MetaResp<BlockInternetTimetable>, GetTimetableArgs> {
  /**
   * @returns The block internet timetable for the account/device.
   * @param opts The device.
   */
  handle(opts: GetTimetableArgs): Promise<MetaResp<BlockInternetTimetable>> {
    return this.authReq({
      url: '/v2/block-internet/timetable',
      method: 'GET',
      params: opts
    })
  }
}
