import Request, { MetaResp } from "../request";
import { BlockInternetDeadline } from "./get-deadline-self-service";

export default class PostDeadline extends Request<MetaResp<null>, BlockInternetDeadline> {
  handle(opts: BlockInternetDeadline): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/block-internet/deadline',
      method: 'POST',
      data: opts
    })
  }
}
