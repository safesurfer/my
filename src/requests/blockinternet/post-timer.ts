import Request, { MetaResp } from "../request"
import { BlockInternetTimer } from "./get-timer"

export interface PostTimerArgs {
  timer: BlockInternetTimer;
  day: string;
}

export default class PostTimer extends Request<MetaResp<null>, PostTimerArgs> {
  /**
   * Set the block internet timer for the device.
   * @param opts 
   */
  handle(opts: PostTimerArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/block-internet/timers/' + opts.day,
      method: 'POST',
      data: opts.timer
    })
  }
}