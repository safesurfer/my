import Request, { MetaResp } from "../request";

export interface PutBlockInternetTimerRunningSelfServiceArgs {
  'device-id': string;
  'mac': string;
  'token': string;
  'day': string;
  'running': boolean;
}

export default class PutTimerRunningSelfService extends Request<MetaResp<null>, PutBlockInternetTimerRunningSelfServiceArgs> {
  handle(opts: PutBlockInternetTimerRunningSelfServiceArgs): Promise<MetaResp<null>> {
    return this.simpleReq({
      url: '/v2/block-internet/timers/' + opts.day + '/self-service/running',
      method: 'PUT',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      },
      data: this.encodeAsForm({
        running: opts.running
      })
    })
  }
}
