import Request, { MetaResp } from "../request";
import { BlockInternetTimetable } from "./get-timetable";

export default class PutTimetable extends Request<MetaResp<null>, BlockInternetTimetable> {
  /**
   * Set the block internet timetable for the device.
   * @param opts 
   */
  handle(opts: BlockInternetTimetable): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/block-internet/timetable',
      method: 'PUT',
      data: opts
    })
  }
}
