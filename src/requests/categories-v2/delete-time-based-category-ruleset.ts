import Request, { MetaResp } from "../request";

export default class DeleteTimeBasedCategoryRuleset extends Request<MetaResp<null>, number> {
  /**
   * Delete a time based ruleset by ID.
   * @param opts 
   */
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/timetables/' + opts,
      method: 'DELETE'
    })
  }
}
