import Request, { MetaResp } from "../request";
import { GetTimeBasedCategoryRulesetsArgs } from "./get-time-based-category-rulesets";

export default class DeleteTimeBasedCategoryRulesets extends Request<MetaResp<null>, GetTimeBasedCategoryRulesetsArgs> {
  /**
   * Delete all time-based category rules for the device.
   * @param opts The device.
   */
  handle(opts: GetTimeBasedCategoryRulesetsArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/timetables',
      method: 'DELETE',
      params: opts
    })
  }
}
