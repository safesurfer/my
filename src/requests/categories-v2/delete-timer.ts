import Request, { MetaResp } from "../request";

export default class DeleteTimer extends Request<MetaResp<null>, number> {
  /**
   * Delete a timer by ID.
   */
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/timers/' + opts,
      method: 'DELETE'
    })
  }
}
