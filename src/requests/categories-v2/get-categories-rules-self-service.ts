import Request, { MetaResp } from "../request";
import { CategoriesRules } from "./get-categories-rules";

export interface GetCategoriesRulesSelfServiceArgs {
  'device-id': string;
  'mac': string;
  token: string;
}

export default class GetCategoriesRulesSelfService extends Request<MetaResp<CategoriesRules>, GetCategoriesRulesSelfServiceArgs> {
  /**
   * Get the categories rules for a device via self-service token.
   */
  handle(opts: GetCategoriesRulesSelfServiceArgs): Promise<MetaResp<CategoriesRules>> {
    return this.simpleReq({
      url: '/v2/categories/rules/self-service',
      method: 'GET',
      auth: {
        username: 'self-service-token',
        password: opts.token
      },
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      }
    })
  }
}
