import Request, { MetaResp } from '@/requests/request'

export interface Category {
  id: number;
  displayName: string;
  icon: string;
  action: number;
  isSite: boolean;
  defaultAlert: boolean;
  frozen: boolean;
  parent: number;
  description: string;
  ogIdx: number; // Not part of API spec, just used for working
}

export interface Restriction {
  id: number;
  displayName: string;
  icon: string;
  actions: string[];
  action: number;
}

export interface App {
  platform: string;
  id: string;
  blocked: boolean;
  installedOn: string[];
  name: string;
  categories: number[];
  restrictions: number[];
  mappedCategoryId: number;
}

export interface CategoriesRules {
  apps: App[];
  categories: Category[];
  restrictions: Restriction[];
  hasRules: boolean;
  whitelist: string[];
  blacklist: string[];
}

export interface GetCategoriesRulesOptions {
  'device-id': string;
  mac: string;
}

export default class GetCategoriesRules extends Request<MetaResp<CategoriesRules>, GetCategoriesRulesOptions> {
  /**
   * @returns The categories rules for a certain device or account.
   * @param opts The device params, which may be left as empty strings to return account rules.
   */
  handle(opts: GetCategoriesRulesOptions): Promise<MetaResp<CategoriesRules>> {
    return this.authReq({
      url: '/v2/categories/rules',
      method: 'GET',
      params: opts
    })
  }
}