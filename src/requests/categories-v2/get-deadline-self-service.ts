import { GetDeadlineSelfServiceArgs as BlockInternetArgs } from "../blockinternet/get-deadline-self-service";
import Request, { MetaResp } from "../request";
import { CategoriesRules } from "./get-categories-rules";

export interface GetDeadlineSelfServiceArgs extends BlockInternetArgs {
  id: number;
}

export interface Deadline {
  id: number;
  deviceID: string;
  mac: string;
  customRules: CategoriesRules;
  expiry: number;
}

export default class GetDeadlineSelfService extends Request<MetaResp<Deadline>, GetDeadlineSelfServiceArgs> {
  handle(opts: GetDeadlineSelfServiceArgs): Promise<MetaResp<Deadline>> {
    return this.simpleReq({
      url: '/v2/categories/deadlines/' + opts.id + '/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
