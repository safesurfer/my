import Request, { MetaResp } from "../request";
import { Deadline } from "./get-deadline-self-service";

export default class GetDeadline extends Request<MetaResp<Deadline>, number> {
  handle(opts: number): Promise<MetaResp<Deadline>> {
    return this.authReq({
      url: '/v2/categories/deadlines/' + opts,
      method: 'GET'
    })
  }
}
