import { GetDeadlineSelfServiceArgs } from "../blockinternet/get-deadline-self-service";
import Request, { MetaResp } from "../request";
import { Deadline } from "./get-deadline-self-service";

export default class GetDeadlinesSelfService extends Request<MetaResp<Deadline[]>, GetDeadlineSelfServiceArgs> {
  handle(opts: GetDeadlineSelfServiceArgs): Promise<MetaResp<Deadline[]>> {
    return this.simpleReq({
      url: '/v2/categories/deadlines/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        mac: opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
