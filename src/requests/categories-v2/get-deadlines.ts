import Request, { MetaResp } from "../request";
import { Deadline } from "./get-deadline-self-service";

export interface GetDeadlinesArgs {
  'device-id': string;
  'mac': string;
}

export default class GetDeadlines extends Request<MetaResp<Deadline[]>, GetDeadlinesArgs> {
  handle(opts: GetDeadlinesArgs): Promise<MetaResp<Deadline[]>> {
    return this.authReq({
      url: '/v2/categories/deadlines',
      method: 'GET',
      params: opts
    })
  }
}
