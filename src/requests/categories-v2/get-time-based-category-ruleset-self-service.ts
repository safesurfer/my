import Request, { MetaResp } from "../request";
import { TimeBasedRules } from "./post-time-based-category-ruleset";

export interface GetTimeBasedCategoryRulesetSelfServiceArgs {
  id: number;
  'device-id': string;
  'mac': string;
  token: string;
}

export default class GetTimeBasedCategoryRulesetSelfService extends Request<MetaResp<TimeBasedRules>, GetTimeBasedCategoryRulesetSelfServiceArgs> {
  /**
   * Get a time based ruleset via self service token.
   */
  handle(opts: GetTimeBasedCategoryRulesetSelfServiceArgs): Promise<MetaResp<TimeBasedRules>> {
    return this.simpleReq({
      url: '/v2/categories/timetables/' + opts.id + '/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
