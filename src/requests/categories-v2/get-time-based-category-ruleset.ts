import Request, { MetaResp } from "../request";
import { TimeBasedRules } from "./post-time-based-category-ruleset";

export default class GetTimeBasedCategoryRuleset extends Request<MetaResp<TimeBasedRules>, number> {
  /**
   * @returns A time-based category ruleset given its ID.
   * @param opts the id.
   */
  handle(opts: number): Promise<MetaResp<TimeBasedRules>> {
    return this.authReq({
      url: '/v2/categories/timetables/' + opts,
      method: 'GET'
    })
  }
}
