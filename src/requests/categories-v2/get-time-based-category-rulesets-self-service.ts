import Request, { MetaResp } from "../request";
import { GetCategoriesRulesSelfServiceArgs } from "./get-categories-rules-self-service";
import { TimeBasedRules } from "./post-time-based-category-ruleset";

export default class GetTimeBasedCategoryRulesetsSelfService extends Request<MetaResp<TimeBasedRules[]>, GetCategoriesRulesSelfServiceArgs> {
  /**
   * Get the time based rulesets via self service token.
   */
  handle(opts: GetCategoriesRulesSelfServiceArgs): Promise<MetaResp<TimeBasedRules[]>> {
    return this.simpleReq({
      url: '/v2/categories/timetables/self-service',
      method: 'GET',
      auth: {
        username: 'self-service-token',
        password: opts.token
      },
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      }
    })
  }
}
