import Request, { MetaResp } from "../request";
import { TimeBasedRules } from "./post-time-based-category-ruleset";

export interface GetTimeBasedCategoryRulesetsArgs {
  'device-id': string;
  'mac': string;
}

export default class GetTimeBasedCategoryRulesets extends Request<MetaResp<TimeBasedRules[]>, GetTimeBasedCategoryRulesetsArgs> {
  /**
   * @returns The IDs of the time-based rulesets for the device.
   * @param opts The device.
   */
  handle(opts: GetTimeBasedCategoryRulesetsArgs): Promise<MetaResp<TimeBasedRules[]>> {
    return this.authReq({
      url: '/v2/categories/timetables',
      method: 'GET',
      params: opts
    })
  }
}
