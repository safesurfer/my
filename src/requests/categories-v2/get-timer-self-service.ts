import Request, { MetaResp } from "../request";
import { Timer } from "./get-timer";

export interface GetTimerSelfServiceArgs {
  id: number;
  'device-id': string;
  mac: string;
  token: string;
}

export default class GetTimerSelfService extends Request<MetaResp<Timer>, GetTimerSelfServiceArgs> {
  /**
   * Get a timer via self-service token.
   */
  handle(opts: GetTimerSelfServiceArgs): Promise<MetaResp<Timer>> {
    return this.simpleReq({
      url: '/v2/categories/timers/' + opts.id + '/self-service',
      method: 'GET',
      auth: {
        username: 'self-service-token',
        password: opts.token
      },
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      }
    })
  }
}

