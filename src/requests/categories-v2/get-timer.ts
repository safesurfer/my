import { BlockInternetTimer } from "../blockinternet/get-timer";
import Request, { MetaResp } from "../request";
import { CategoriesRules } from "./get-categories-rules";

export interface Timer extends BlockInternetTimer {
  id: number;
  customRules: CategoriesRules;
  day: string;
}

export default class GetTimer extends Request<MetaResp<Timer>, number> {
  /**
   * Get a timer by ID.
   */
  handle(opts: number): Promise<MetaResp<Timer>> {
    return this.authReq({
      url: '/v2/categories/timers/' + opts,
      method: 'GET'
    })
  }
}
