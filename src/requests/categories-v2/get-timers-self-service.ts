import Request, { MetaResp } from "../request";
import { GetCategoriesRulesSelfServiceArgs } from "./get-categories-rules-self-service";
import { Timer } from "./get-timer";

export default class GetTimersSelfService extends Request<MetaResp<Timer[]>, GetCategoriesRulesSelfServiceArgs> {
  /**
   * Get the timer IDs for a device.
   */
  handle(opts: GetCategoriesRulesSelfServiceArgs): Promise<MetaResp<Timer[]>> {
    return this.simpleReq({
      url: '/v2/categories/timers/self-service',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}