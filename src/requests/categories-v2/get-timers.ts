import Request, { MetaResp } from "../request";
import { GetTimeBasedCategoryRulesetsArgs } from "./get-time-based-category-rulesets";
import { Timer } from "./get-timer";

export default class GetTimers extends Request<MetaResp<Timer[]>, GetTimeBasedCategoryRulesetsArgs> {
  /**
   * Get the timer IDs for a device.
   */
  handle(opts: GetTimeBasedCategoryRulesetsArgs): Promise<MetaResp<Timer[]>> {
    return this.authReq({
      url: '/v2/categories/timers',
      method: 'GET',
      params: opts
    })
  }
}
