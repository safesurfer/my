import Request, { MetaResp } from "../request";
import { GetCategoriesRulesSelfServiceArgs } from "./get-categories-rules-self-service";

export default class GetTimezoneSelfService extends Request<MetaResp<string>, GetCategoriesRulesSelfServiceArgs> {
  /**
   * Get the timezone via self-service token.
   */
  handle(opts: GetCategoriesRulesSelfServiceArgs): Promise<MetaResp<string>> {
    return this.simpleReq({
      url: '/v2/categories/self-service-timezone',
      method: 'GET',
      params: {
        'device-id': opts['device-id'],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts['token']
      }
    })
  }
}
