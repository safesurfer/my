import Request, { MetaResp } from '@/requests/request'
import { CategoriesRules } from './get-categories-rules'

export interface PatchCategoriesRulesOptions {
  'device-id': string;
  mac: string;
  rules: CategoriesRules;
}

export default class PatchCategoriesRules extends Request<MetaResp<null>, PatchCategoriesRulesOptions> {
  /**
   * Set the categories rules for an account or device.
   */
  handle(opts: PatchCategoriesRulesOptions): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/rules',
      method: 'PATCH',
      params: {
        'device-id': opts['device-id'],
        mac: opts.mac
      },
      data: opts.rules
    })
  }
}
