import Request, { MetaResp } from "../request";
import { Deadline } from "./get-deadline-self-service";

export default class PostDeadline extends Request<MetaResp<number>, Deadline> {
  handle(opts: Deadline): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/categories/deadlines',
      method: 'POST',
      data: opts
    })
  }
}
