import { BlockInternetTimetable } from "../blockinternet/get-timetable";
import Request, { MetaResp } from "../request"
import { CategoriesRules } from "./get-categories-rules";

export interface TimeBasedRules extends BlockInternetTimetable {
  id: number;
  customRules: CategoriesRules;
}

export default class PostTimeBasedCategoryRuleset extends Request<MetaResp<number>, TimeBasedRules> {
  /**
   * Add the time-based rules.
   * @param opts The rules to add and for which device.
   */
  handle(opts: TimeBasedRules): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/categories/timetables',
      method: 'POST',
      params: {
        'device-id': opts.deviceID,
        'mac': opts.mac
      },
      data: opts
    })
  }
}
