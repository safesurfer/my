import Request, { MetaResp } from "../request";
import { Timer } from "./get-timer";

export default class PostTimer extends Request<MetaResp<number>, Timer> {
  /**
   * Add a timer, return the ID created for it.
   */
  handle(opts: Timer): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/categories/timers',
      method: 'POST',
      data: opts
    })
  }
}
