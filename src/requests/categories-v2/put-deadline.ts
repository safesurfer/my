import Request, { MetaResp } from "../request";
import { Deadline } from "./get-deadline-self-service";

export default class PutDeadlineRule extends Request<MetaResp<null>, Deadline> {
  handle(opts: Deadline): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/deadlines/' + opts.id,
      method: 'PUT',
      data: opts
    })
  }
}
