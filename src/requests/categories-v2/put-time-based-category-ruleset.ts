import Request, { MetaResp } from "../request";
import { TimeBasedRules } from "./post-time-based-category-ruleset";

export default class PutTimeBasedCategoryRuleset extends Request<MetaResp<null>, TimeBasedRules> {
  /**
   * Update the time based rules.
   * @param opts The new time based rules.
   */
  handle(opts: TimeBasedRules): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/timetables/' + opts.id,
      method: 'PUT',
      data: opts
    })
  }
}
