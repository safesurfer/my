import Request, { MetaResp } from "../request";

export interface PutTimerRunningSelfServiceArgs {
  id: number;
  'device-id': string;
  'mac': string;
  running: boolean;
  token: string;
}

export default class PutTimerRunningSelfService extends Request<MetaResp<null>, PutTimerRunningSelfServiceArgs> {
  /**
   * Set whether the timer is running via self-service.
   */
  handle(opts: PutTimerRunningSelfServiceArgs): Promise<MetaResp<null>> {
    return this.simpleReq({
      url: '/v2/categories/timers/' + opts.id + '/self-service/running',
      method: 'PUT',
      data: this.encodeAsForm({
        running: opts.running
      }),
      params: {
        'device-id': opts["device-id"],
        'mac': opts['mac']
      },
      auth: {
        username: 'self-service-token',
        password: opts.token
      }
    })
  }
}
