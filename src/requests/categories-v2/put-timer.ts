import Request, { MetaResp } from "../request";
import { Timer } from "./get-timer";

export default class PutTimer extends Request<MetaResp<null>, Timer> {
  /**
   * Update a timer.
   */
  handle(opts: Timer): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/categories/timers/' + opts.id,
      method: 'PUT',
      data: opts
    })
  }
}
