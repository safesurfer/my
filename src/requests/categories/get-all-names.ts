import Request from '@/requests/request'

export interface CategoryName {
  /**
   * The unique ID for the category.
   */
  id: number;
  /**
   * The name of the category.
   */
  name: string;
  /**
   * Whether the category alerts by default.
   */
  defaultAlert: boolean;
}

export default class GetAllNames extends Request<CategoryName[], undefined> {
  handle (): Promise<CategoryName[]> {
    return this.authReq({
      url: '/categories/names',
      method: 'GET'
    })
  }
}
