import Request from '@/requests/request'

/**
 * Return value.
 */
export interface BlockedCategories {
  /**
   * An array of the category IDs which are blocked
   * for the device.
   */
  blocked: number[];
  /**
   * Whether the device has device specific settings enabled.
   * Not meaningful for the global device, which is always
   * enabled.
   */
  overrideEnabled: boolean;
}

export interface GetBlockedCategoriesOptions {
  mac?: string;
  lifeguardID?: string;
}

export default class GetBlockedCategories extends Request<BlockedCategories, GetBlockedCategoriesOptions> {
  /**
   * Given a device MAC, return which categories are blocked for that device.
   * @param opts Object containing device MAC, or 'GLOBAL' in place of the MAC to get the rules for all devices,
   *              and lifeguard ID.
   */
  handle (opts: GetBlockedCategoriesOptions): Promise<BlockedCategories> {
    return this.authReq({
      url: '/categories/blocked/',
      method: 'GET',
      params: {
        id: opts.lifeguardID,
        mac: opts.mac
      }
    })
  }
}
