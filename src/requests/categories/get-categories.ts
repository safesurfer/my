import Request from '@/requests/request'

export interface CategoriesDetails {
  /**
   * Map from category IDs to display name.
   */
  displayNames: {[catID: string]: string};
  /**
   * Optionally, an MDI icon for each category by ID.
   * If not defined, uses a placeholder.
   */
  icons: {[catID: string]: string};
}

export default class GetCategories extends Request<CategoriesDetails, undefined> {
  /**
   * Whether to redirect if unauthenticated.
   */
  unauthRedirect: boolean

  constructor (unauthRedirect: boolean) {
    super()
    this.unauthRedirect = unauthRedirect
  }

  protected redirectOn401 (): boolean {
    return this.unauthRedirect
  }

  /**
   * @retuens A promise resolving with the categories of sites
   *          that can be blocked.
   */
  handle (): Promise<CategoriesDetails> {
    return this.authReq({
      url: '/categories/all',
      method: 'GET'
    })
  }
}
