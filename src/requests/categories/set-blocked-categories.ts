import Request from '@/requests/request'

export interface SetBlockedCategoriesOptions {
  /**
   * Device MAC to set the categories for, or 'GLOBAL'
   * for the rules for all devices.
   */
  deviceMac?: string;
  /**
   * The lifeguard device ID.
   */
  deviceID?: string;
  /**
   * An array of the category IDs which should be blocked
   * for the device.
   */
  blocked: number[];
  /**
   * An array of category IDs which should not be blocked
   * for the device.
   */
  notBlocked: number[];
  /**
   * Whether the device should have device specific settings enabled.
   * Not meaningful for the global device, which is always
   * enabled.
   */
  overrideEnabled: boolean;
}

export default class SetBlockedCategories extends Request<undefined, SetBlockedCategoriesOptions> {
  /**
   * Set the blocked category rules for a device.
   * @param opts Object containing device MAC, the categories to be blocked, and whether
   *              device specific rules are enabled.
   */
  handle (opts: SetBlockedCategoriesOptions): Promise<undefined> {
    return this.authReq({
      url: '/categories/blocked/',
      method: 'POST',
      data: opts
    })
  }
}
