import { getCategorizerURL } from "@/helpers/functions"
import Request, { MetaResp } from "@/requests/request"

export interface Category {
  id: number;
  name: string;
  defaultAction: number;
  defaultAlert: boolean;
  description: string;
  frozen: boolean;
  icon: string;
  isSite: boolean;
  parent: number;
  display: boolean;
}

export default class GetAllCategories extends Request<MetaResp<Category[]>, undefined> {
  /**
   * @returns The list of all categories from the categorizer.
   */
  handle(): Promise<MetaResp<Category[]>> {
    return this.simpleReq({
      url: '/api/categories',
      method: 'GET'
    })
  }

  getBaseURL () {
    return getCategorizerURL()
  }
}
