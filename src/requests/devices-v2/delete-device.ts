import Request, { MetaResp } from '../request'

export interface DeleteDeviceArgs {
  id: string;
  mac: string;
}

export default class DeleteDevice extends Request<MetaResp<null>, DeleteDeviceArgs>{
  /**
   * Delete a device by its ID. Only lifeguards or dns devices are supported.
   * @param opts 
   */
  handle(opts: DeleteDeviceArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + encodeURIComponent(opts.id),
      method: 'DELETE',
      params: {
        'mac': opts.mac
      }
    })
  }
}
