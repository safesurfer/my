import Request, { MetaResp } from "../request";

export interface DeleteMetadataArgs {
  deviceId: string;
  key: string;
}

export default class DeleteMetadata extends Request<MetaResp<null>, DeleteMetadataArgs> {
  handle(opts: DeleteMetadataArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + opts.deviceId + '/metadata/' + opts.key,
      method: 'DELETE'
    })
  }
}
