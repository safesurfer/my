import Request from "../request";

export interface GetAppleMobileConfigQrArgs {
  'dns-token': string;
  'profile-name': string;
  'removal-password': string;
}

export default class GetAppleMobileConfigQr extends Request<Blob, GetAppleMobileConfigQrArgs> {
  handle(opts: GetAppleMobileConfigQrArgs): Promise<Blob> {
    return this.simpleReq({
      url: '/v2/devices/apple-mobileconfig/qr',
      method: 'GET',
      responseType: 'blob',
      params: opts
    })
  }
}
