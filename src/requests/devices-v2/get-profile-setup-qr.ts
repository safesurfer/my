import Request from "../request";

export interface GetProfileSetupQrArgs {
  type: string;
  token: string;
  'blocked-categories'?: string;
}

export default class GetProfileSetupQr extends Request<Blob, GetProfileSetupQrArgs> {
  handle(opts: GetProfileSetupQrArgs): Promise<Blob> {
    return this.simpleReq({
      url: '/v2/devices/profile-setup-qr',
      method: 'GET',
      responseType: 'blob',
      params: opts
    })
  }
}
