import { GetCategoriesRulesOptions } from "../categories-v2/get-categories-rules";
import Request, { MetaResp } from "../request";

export default class GetSelfServiceToken extends Request<MetaResp<string>, GetCategoriesRulesOptions> {
  handle(opts: GetCategoriesRulesOptions): Promise<MetaResp<string>> {
    return this.authReq({
      url: '/v2/devices/self-service-token',
      method: 'GET',
      params: opts
    })
  }
}
