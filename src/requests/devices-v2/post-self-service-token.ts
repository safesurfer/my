import { GetCategoriesRulesOptions } from "../categories-v2/get-categories-rules";
import Request, { MetaResp } from "../request";

export default class PostSelfServiceToken extends Request<MetaResp<null>, GetCategoriesRulesOptions> {
  handle(opts: GetCategoriesRulesOptions): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/self-service-token',
      method: 'POST',
      params: opts
    })
  }
}
