import { getBaseURLIpv6 } from "@/helpers/functions";
import Request, { MetaResp } from "../request";

export default class PutDeviceIp6 extends Request<MetaResp<null>, string> {
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/devices/' + opts + '/ip6',
      method: 'PUT'
    })
  }

  protected getBaseURL(): string {
    return getBaseURLIpv6()
  }
}
