import Request from '@/requests/request'

export default class GetDeviceDNSToken extends Request<string, string> {
  /**
   * @returns The dns token for a device.
   * @param deviceID The device ID of the device.
   */
  handle (deviceID: string): Promise<string> {
    return this.authReq({
      url: '/devices/device/' + encodeURIComponent(deviceID) + '/dns-token',
      method: 'GET'
    })
  }
}
