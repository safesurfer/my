import Request from '../request'

export default class GetDeviceName extends Request<string, string> {
  /**
   * @returns The name of a device given its ID.
   * @param opts The device ID, or "-" to infer from device-specific JWT if any.
   */
  handle(opts: string): Promise<string> {
    return this.authReq({
      url: '/devices/device/' + encodeURIComponent(opts) + '/name',
      method: 'GET'
    })
  }
}
