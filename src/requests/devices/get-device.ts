import Request from "../request";
import { APIDevice } from "./get-devices"

export interface GetDeviceArgs {
  'device-id': string;
  'mac'?: string;
}

export default class GetDevice extends Request<APIDevice, GetDeviceArgs> {
  handle(opts: GetDeviceArgs): Promise<APIDevice> {
    return this.authReq({
      url: '/devices/device',
      method: 'GET',
      params: opts
    })
  }
}
