import Request from '@/requests/request'

export interface APIDevices {
  /**
   * The lifeguard devices owned by the user.
   */
  lifeguards: APILifeguardDevice[];
  /**
   * The mobile devices for the user.
   */
  mobile: APIDevice[];
  /**
   * The dns devices for the user.
   */
  dns: APIDevice[];
}

/**
 * Describes some kind of device the user owns.
 */
export interface APIDevice {
  /**
   * Name of the device - for a lifeguard, this is either a nickname
   * or a placeholder e.g. "Lifeguard 1". For a device on a lifeguard
   * network, this is how it identified itself on the network, or
   * the user's nickname for the device. For a mobile device, this
   * is the name of the model of the phone and can't be changed.
   * For a DOH device, this is the generated name or nickname.
   */
  name: string;
  /**
   * Unix timestamp of when the device was last updated.
   */
  lastUpdated: number;
  /**
   * Timestamp of when the device was registered.
   */
  registeredTime: number;
  /**
   * Last known IP of the device. Only relevant for a lifeguard
   * or mobile device.
   */
  ip: string;
  /**
   * The MAC of the device. Only relevant for a device on a lifeguard network.
   */
  mac: string;
  /**
   * The ID for a lifeguard device, mobile device, or DOH device. Not relevant
   * for a device on a lifeguard network.
   */
  id: string;
  /**
   * The type of the device. android-device, ios-device, or windows-device.
   */
  type: 'android-device' | 'ios-device' | 'windows-device' | 'dns-device' | 'op-device';
  /**
   * The device's DNS token.
   */
  dnsToken: string;
  /**
   * The device's current/updating version.
   */
  version: string;
  nextVersion: string;
  metadata?: {[key: string]: string}
}

/**
 * Describes a lifeguard.
 */
export interface APILifeguardDevice extends APIDevice {
  /**
   * The devices on the lifeguard's network.
   */
  devices: APIDevice[];
}

export default class GetDevices extends Request<APIDevices, undefined> {
  /**
   * @returns The devices registered to the current user.
   */
  handle (): Promise<APIDevices> {
    return this.authReq({
      url: '/devices'
    })
  }
}
