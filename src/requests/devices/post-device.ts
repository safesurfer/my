import Request from '@/requests/request'

export type NewDeviceType = 'lifeguard' | 'mobile' | 'dns' | 'desktop'
export type NewDeviceOs = 'ios' | 'android' | 'windows' | ''

export interface PostDeviceOptions {
  type: NewDeviceType;
  // The name to give the device.
  name: string;
  os: NewDeviceOs;
}

export default class PostDevice extends Request<string, PostDeviceOptions> {
  /**
   * @returns The device ID of a newly created device.
   * @param opts Request options.
   */
  handle (opts: PostDeviceOptions): Promise<string> {
    return this.authReq({
      url: '/devices',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }
}
