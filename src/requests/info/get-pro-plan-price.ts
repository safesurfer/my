import Request from "../request"

export interface PlanPrice {
  price: string;
  trial: string;
  trialPeriod: number;
  trialPeriodUnit: string;
  period: string;
  currencySymbol: string;
  currencyCode: string;
  numericPrice: number;
  addons: {[name: string]: Addon};
}

export interface Addon {
  baseQuantity: number;
  pricingTiers: PricingTier[];
}

export interface PricingTier {
  min: number;
  max: number;
  price: number;
}

export default class GetPlanPrice extends Request<PlanPrice, string> {
  /**
   * @returns The price of a plan (+ addons) for the user's region.
   */
  handle (plan: string): Promise<PlanPrice> {
    return this.simpleReq({
      url: '/info/plans/' + plan + '/price',
      method: 'GET'
    })
  }
}
