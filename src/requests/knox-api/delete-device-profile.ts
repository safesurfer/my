import { getKnoxApiUrl } from "@/helpers/functions";
import Request, { MetaResp } from "../request";

export default class DeleteDeviceProfile extends Request<MetaResp<null>, string> {
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/device/' + opts + '/profile',
      method: 'DELETE'
    })
  }

  getBaseURL () {
    return getKnoxApiUrl()
  }
}
