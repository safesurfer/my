import { getKnoxApiUrl } from "@/helpers/functions";
import Request, { MetaResp } from "../request";

export interface PostDeviceArgs {
  'custom-id'?: string;
  serial?: string;
  imei?: string;
  'form-factor'?: string;
  name: string;
}

export interface PostDeviceResponse {
  createdId: string;
  metadata: {[key: string]: string};
}

export default class PostDevice extends Request<MetaResp<PostDeviceResponse>, PostDeviceArgs> {
  handle(opts: PostDeviceArgs): Promise<MetaResp<PostDeviceResponse>> {
    return this.authReq({
      url: '/device',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }

  getBaseURL () {
    return getKnoxApiUrl()
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
