import { getKnoxApiUrl } from "@/helpers/functions";
import Request, { MetaResp } from "../request";

export interface QrRequestArgs {
  serial: string;
  imei: string;
  profile: string;
}

export interface QrResponse {
  expiry: number;
}

export default class PostQrRequest extends Request<MetaResp<QrResponse>, QrRequestArgs> {
  handle(opts: QrRequestArgs): Promise<MetaResp<QrResponse>> {
    return this.authReq({
      url: '/qr/request',
      method: 'POST',
      data: opts
    })
  }

  getBaseURL () {
    return getKnoxApiUrl()
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}