import { getKnoxApiUrl } from "@/helpers/functions";
import Request, { MetaResp } from "../request";

export type Profile = 'basic' | 'intermediate' | 'advanced' | 'basic-tablet' | 'intermediate-tablet' | 'advanced-tablet'

export interface PutDeviceProfileArgs {
  deviceId: string;
  profile: Profile;
  force?: boolean;
}

export default class PutDeviceProfile extends Request<MetaResp<null>, PutDeviceProfileArgs> {
  handle(opts: PutDeviceProfileArgs): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/device/' + opts.deviceId + '/profile',
      method: 'PUT',
      data: opts.profile,
      params: opts.force ? {
        force: true
      } : null,
      headers: {'Content-Type': 'application/json'}
    })
  }

  getBaseURL () {
    return getKnoxApiUrl()
  }
}
