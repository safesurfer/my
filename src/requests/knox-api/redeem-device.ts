import { getKnoxApiUrl } from "@/helpers/functions";
import Request, { MetaResp } from "../request";

export type SubscriptionState = 'DISCOUNT_APPLIED' | 'ANNUAL_READY' | 'DISCOUNT_NOT_APPLIED' | 'CARD_READY' | 'NO_CHANGE' | 'UNKNOWN_ISSUE' | 'GOOGLE_PLAY_ISSUE' | 'APPLE_ISSUE' | 'COURTESY_ISSUE' | 'REACTIVATE_READY'

export interface SubscriptionStatus {
  state: SubscriptionState;
}

export default class RedeemDevice extends Request<MetaResp<SubscriptionStatus>, undefined> {
  handle(): Promise<MetaResp<SubscriptionStatus>> {
    return this.authReq({
      url: '/device/redeem',
      method: 'POST',
    })
  }

  getBaseURL () {
    return getKnoxApiUrl()
  }
}
