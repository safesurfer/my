import Request, { MetaResp } from "../request";

export default class DeleteAllLocationHistory extends Request<MetaResp<null>, string> {
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/location/history/delete-all',
      method: 'POST',
      params: {
        'device-id': opts
      }
    })
  }
}
