import Request, { MetaResp } from "../request";

export default class DeleteLocationHistory extends Request<MetaResp<null>, number[]> {
  handle(opts: number[]): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/location/history/delete',
      method: 'POST',
      data: opts
    })
  }
}
