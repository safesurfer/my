import Request, { MetaResp } from "../request";

export interface GetLocationHistoryArgs {
  'device-id': string;
  'start-timestamp'?: number;
  'start-date'?: string;
  tz?: string;
  'end-timestamp'?: number;
  'end-date'?: string;
}

export const NetworkTypeUnknown = 0
export const NetworkTypeMobile = 1
export const NetworkTypeWifi = 2

export interface Location {
  id: number;
  lat: number;
  lon: number;
  time: number;
  networkType: number;
  accuracy: number;
}

export interface LocationHistory {
  devices: {[deviceId: string]: Location[]}
}

export default class GetLocationHistory extends Request<MetaResp<LocationHistory>, GetLocationHistoryArgs> {
  handle(opts: GetLocationHistoryArgs): Promise<MetaResp<LocationHistory>> {
    return this.authReq({
      url: '/v2/location/history',
      method: 'GET',
      params: opts
    })
  }
}
