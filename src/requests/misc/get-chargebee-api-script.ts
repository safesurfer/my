import Request from '@/requests/request'

export default class GetChargebeeAPIScript extends Request<string, undefined> {
  /**
   * Get the chargebee script.
   */
  handle(): Promise<string> {
    return this.simpleReq({
      url: '',
      method: 'GET'
    })
  }

  getBaseURL (): string {
    return 'https://js.chargebee.com/v2/chargebee.js'
  }
}
