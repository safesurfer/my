import Request from '@/requests/request'

export default class PostSubscription extends Request<undefined, PushSubscription> {
  /**
   * Create a new subscription.
   * @param opts The subscription as returned by PushManager.subscribe()
   */
  handle(opts: PushSubscription): Promise<undefined> {
    return this.authReq({
      url: '/notifications/web-push/subscriptions',
      method: 'POST',
      data: opts
    })
  }
}
