import Request, { MetaResp } from "../request"

export default class GetNotificationApps extends Request<MetaResp<string[]>, undefined> {
  /**
   * @returns The apps that have logged notifications for the user.
   */
  handle(): Promise<MetaResp<string[]>> {
    return this.authReq({
      url: '/v2/notifications-log/notifications/apps',
      method: 'GET'
    })
  }
}
