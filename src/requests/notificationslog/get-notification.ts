import Request, { MetaResp } from "../request"

export interface LoggedNotification {
  appID: string;
  content: string;
  deviceID: string;
  id: number;
  keywords: string[];
  timestamp: number;
  title: string;
}

export default class GetNotification extends Request<MetaResp<LoggedNotification>, number> {
  /**
   * Get a single notification.
   * @param opts The notification ID.
   */
  handle(opts: number): Promise<MetaResp<LoggedNotification>> {
    return this.authReq({
      url: '/v2/notifications-log/notifications/' + opts,
      method: 'GET'
    })
  }
}
