import Request, { MetaResp } from "../request"
import { LoggedNotification } from "./get-notification"

export interface LoggedNotifications {
  more: boolean;
  notifications: LoggedNotification[];
}

export interface GetNotificationsParams {
  'device-id': string;
  'start-timestamp': number;
  'end-timestamp': number;
  'app-id': string[];
  keyword: string[];
  page: number;
  'num-per-page': number;
}

export default class GetNotifications extends Request<MetaResp<LoggedNotifications>, GetNotificationsParams> {
  /**
   * @returns A set of notifications given filters.
   * @param opts The filters.
   */
  handle(opts: GetNotificationsParams): Promise<MetaResp<LoggedNotifications>> {
    return this.authReq({
      url: '/v2/notifications-log/notifications',
      method: 'GET',
      params: opts
    })
  }
}
