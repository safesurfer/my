import { getDnscryptProtoDomain } from '@/helpers/functions'
import Request from '@/requests/request'

export default class GetDNSCrypt extends Request<'Protocol Active', undefined> {
  /**
   * @returns "Protocol Active" if the plain protocol is active.
   */
  handle (): Promise<'Protocol Active'> {
    return this.simpleReq({
      url: 'https://' + getDnscryptProtoDomain(),
      method: 'GET'
    })
  }

  getBaseURL (): string {
    return ''
  }
}
