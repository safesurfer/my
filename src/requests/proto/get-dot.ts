import { getDotProtoDomain } from '@/helpers/functions'
import Request from '@/requests/request'

export default class GetDOT extends Request<'Protocol Active', undefined> {
  /**
   * @returns "Protocol Active" if the dot protocol is active.
   */
  handle (): Promise<'Protocol Active'> {
    return this.simpleReq({
      url: 'https://' + getDotProtoDomain(),
      method: 'GET'
    })
  }

  getBaseURL (): string {
    return ''
  }
}
