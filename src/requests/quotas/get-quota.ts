import Request, { MetaResp } from "../request";

export type QuotaImposedBy = 'default' | 'grant' | 'plan'

export interface Quota {
  maxValue: number;
  currentValue: number;
  imposedBy: QuotaImposedBy
}

export default class GetQuota extends Request<MetaResp<Quota>, string> {
  handle(opts: string): Promise<MetaResp<Quota>> {
    return this.authReq({
      url: '/v2/quotas/' + opts,
      method: 'GET'
    })
  }
}
