import Request from '@/requests/request'

export interface DeleteCastEventOptions {
  /**
   * The ID of the cast the event belongs to.
   */
  castID: number;
  /**
   * The ID of the event.
   */
  eventID: number;
}

export default class DeleteCastEvent extends Request<undefined, DeleteCastEventOptions> {
  /**
   * Delete a cast event by its ID.
   * @param opts The id.
   */
  handle (opts: DeleteCastEventOptions): Promise<undefined> {
    return this.authReq({
      url: '/screencasts/cast/' + opts.castID + '/events/event/' + opts.eventID,
      method: 'DELETE'
    })
  }
}
