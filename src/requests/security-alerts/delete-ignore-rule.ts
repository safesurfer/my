import Request, { MetaResp } from "../request";

export default class DeleteIgnoreRule extends Request<MetaResp<null>, number> {
  handle(opts: number): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/security-alerts/ignore-rules/' + opts,
      method: 'DELETE'
    })
  }
}
