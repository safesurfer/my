import Request, { MetaResp } from "../request";
import { SecurityAlert } from "./get-security-alerts";

export default class DeleteSecurityAlerts extends Request<MetaResp<null>, SecurityAlert> {
  handle(opts: SecurityAlert): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/security-alerts/delete',
      method: 'POST',
      data: opts
    })
  }
}
