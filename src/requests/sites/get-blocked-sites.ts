import Request from '@/requests/request'

export interface GetBlockedSitesOptions {
  /**
   * MAC to get settings for, or "GLOBAL".
   */
  mac?: string;
  lifeguardID?: string;
}

export interface BlockedSites {
  /**
   * How blocked youtube is - not blocked is completely
   * unrestricted, blocked is blocked at a domain level,
   * the others enforce filtering of different strengths.
   */
  youtube: YoutubeSettings;
  /**
   * An array of the site IDs that are blocked for the device.
   * Site names returned by get-site-presets endpoint.
   */
  presetBlocked: number[];
  /**
   * A list of manually whitelisted/blacklisted sites.
   */
  blacklist: string[];
  whitelist: string[];
  /**
   * Whether to override global settings for the device.
   */
  overrideEnabled: boolean;
}

export type YoutubeSettings = 'NOT_BLOCKED' | 'LIGHT_FILTER' | 'STRICT_FILTER' | 'BLOCKED'

export default class GetBlockedSites extends Request<BlockedSites, GetBlockedSitesOptions> {
  /**
   * Get the sites blocked for a particular device.
   * @param opts Object containing the device MAC.
   */
  handle (opts: GetBlockedSitesOptions): Promise<BlockedSites> {
    return this.authReq({
      url: '/sites/blocked/',
      method: 'GET',
      params: {
        id: opts.lifeguardID,
        mac: opts.mac
      }
    })
  }
}
