import Request from '@/requests/request'

export interface SitePresets {
  /**
   * Map from site ID to display name.
   */
  displayNames: {[catID: number]: string};
  /**
   * Optionally, an MDI icon for each site by ID.
   * If not defined, uses a placeholder.
   */
  icons: {[catID: number]: string};
}

export default class GetSitePresets extends Request<SitePresets, undefined> {
  /**
   * Whether to redirect if unauthenticated.
   */
  unauthRedirect: boolean

  constructor (unauthRedirect: boolean) {
    super()
    this.unauthRedirect = unauthRedirect
  }

  protected redirectOn401 (): boolean {
    return this.unauthRedirect
  }
  
  /**
   * Get the sites that may be blocked by the current DNS.
   */
  handle (): Promise<SitePresets> {
    return this.authReq({
      url: '/sites/all',
      method: 'GET'
    })
  }
}
