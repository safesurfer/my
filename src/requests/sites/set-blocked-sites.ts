import { YoutubeSettings } from '@/requests/sites/get-blocked-sites'
import Request from '@/requests/request'

export interface SetBlockedSitesOptions {
  /**
   * MAC of the device to set options for.
   */
  deviceMac?: string;
  /**
   * Lifeguard ID
   */
  lifeguardID?: string;
  /**
   * Account ID, always blank because requesting from
   * lifeguard.
   */
  accountID: '';
  /**
   * How blocked youtube should be - not blocked is completely
   * unrestricted, blocked is blocked at a domain level,
   * the others enforce filtering of different strengths.
   */
  youtube: YoutubeSettings;
  /**
   * An array of the sites that are now blocked for the device.
   */
  presetBlocked: number[];
  /**
   * Sites not blocked for the device.
   */
  presetNotBlocked: number[];
  /**
   * A list of manually whitelisted/blacklisted sites.
   */
  blacklist: string[];
  whitelist: string[];
  /**
   * Whether the settings should override global settings.
   */
  overrideEnabled: boolean;
}

export default class SetBlockedSites extends Request<undefined, SetBlockedSitesOptions> {
  /**
   * Set the blocked sites for a device.
   * @param opts Object containing device mac, youtube blocking, preset blocked sites, and manually blocked sites.
   */
  handle (opts: SetBlockedSitesOptions): Promise<undefined> {
    return this.authReq({
      url: '/sites/blocked/',
      method: 'POST',
      data: opts
    })
  }
}
