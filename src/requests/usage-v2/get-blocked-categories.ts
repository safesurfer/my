import Request, { MetaResp } from "../request"

export interface CategoryCount {
  category: string;
  categoryDescription: string;
  categoryID: number;
  count: number;
  icon: string;
}

export interface GetCategoriesParams {
  'start-timestamp'?: number;
  'start-date'?: string;
  'end-timestamp'?: number;
  'end-date'?: string;
  'tz'?: string;
  'device-id'?: string;
  'mac'?: string;
}

export default class GetBlockedCategories extends Request<MetaResp<CategoryCount[]>, GetCategoriesParams> {
  handle(opts: GetCategoriesParams): Promise<MetaResp<CategoryCount[]>> {
    return this.authReq({
      url: '/v2/usage/blocked-categories',
      method: 'GET',
      params: opts
    })
  }
}
