import Request, { MetaResp } from "../request"

export interface UsageHistory {
  more: boolean;
  history: UsageHistoryItem[];
}

export interface UsageHistoryItem {
  timestamp: number;
  deviceID: string;
  mac: string;
  domain: string;
  categoryIDs: number[];
  categoryNames: number[];
  matchingCategoryID: number;
  action: string;
}

export type GetHistoryAction = 'access' | 'blacklist' | 'redirect' | 'whitelist'
export type GetHistorySort = 'alpha-a-z' | 'alpha-z-a' | 'date-new-first' | 'date-old-first'

export interface GetHistoryParams {
  'start-timestamp'?: number;
  'start-date'?: string;
  'end-timestamp'?: number;
  'end-date'?: string;
  'tz'?: string;
  'device-id'?: string;
  'mac'?: string;
  'domain-search'?: string;
  'category-id'?: number[];
  'action'?: GetHistoryAction;
  'num-per-page'?: number;
  'page'?: number;
  'sort'?: GetHistorySort;
}

export default class GetHistory extends Request<MetaResp<UsageHistory>, GetHistoryParams> {
  handle(opts: GetHistoryParams): Promise<MetaResp<UsageHistory>> {
    return this.authReq({
      url: '/v2/usage/history',
      params: opts
    })
  }
}