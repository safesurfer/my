import Request, { MetaResp } from "../request";

export interface GetLastActivityArgs {
  'device-id'?: string;
  'mac'?: string;
}

export default class GetLastActivity extends Request<MetaResp<number>, GetLastActivityArgs> {
  handle(opts: GetLastActivityArgs): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/usage/last-activity',
      method: 'GET',
      params: opts
    })
  }
}