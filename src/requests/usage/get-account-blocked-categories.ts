import Request from '@/requests/request'

export interface AccountCategories {
  // The categories blocked
  categories: string[];
  // The amount of times blocked for each category
  occurrences: number[];
}

export default class GetAccountBlockedCategories extends Request<AccountCategories, number> {
  /**
   * @returns The blocks for the account by category.
   * @param start A timestamp of when to get usage from.
   */
  handle (start: number): Promise<AccountCategories> {
    return this.authReq({
      url: '/usage/account/blocked-categories',
      method: 'GET',
      params: {
        start: start
      }
    })
  }
}
