import Request from '@/requests/request'
import { AccountCategories } from '@/requests/usage/get-account-blocked-categories'

export default class GetAccountBlockedCategories extends Request<AccountCategories, number> {
  /**
   * @returns The blocks for the account by category.
   * @param start A timestamp of when to get usage from.
   */
  handle (start: number): Promise<AccountCategories> {
    return this.authReq({
      url: '/usage/account/viewed-categories',
      method: 'GET',
      params: {
        start: start
      }
    })
  }
}
