import Request from '@/requests/request'
import { DeviceUsage } from '@/requests/usage/get-lifeguard-device-usage'

export interface GetDeviceUsageOptions {
  /**
   * Start time to get usage from, yyyymmdd.
   */
  start: string;
  /**
   * End time to get usage to, yyyymmdd.
   */
  end: string;
  /**
   * Timezone to get usage for.
   */
  tz: string;
  /**
   * Device ID to get usage for.
   */
  id: string;
}

export default class GetDeviceUsage extends Request<DeviceUsage, GetDeviceUsageOptions> {
  /**
   * Get the usage for a device.
   * @param opts Params.
   */
  handle (opts: GetDeviceUsageOptions): Promise<DeviceUsage> {
    return this.authReq({
      url: '/usage/device',
      method: 'GET',
      params: opts
    })
  }
}
