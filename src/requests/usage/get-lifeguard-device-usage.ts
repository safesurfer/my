import Request from '@/requests/request'

export interface DeviceUsage {
  /**
   * Blocked sites.
   */
  block: {[catName: string]: {[site: string]: number}};
  /**
   * Viewed sites.
   */
  allow: {[catName: string]: number};
}

export interface LifeguardDeviceUsageOptions {
  /**
   * Start time to get usage from, yyyymmdd.
   */
  start: string;
  /**
   * End time to get usage to, yyyymmdd.
   */
  end: string;
  /**
   * Timezone to get usage for.
   */
  tz: string;
  /**
   * MAC of the device to get usage for.
   */
  mac: string;
  /**
   * Lifeguard ID to get usage for.
   */
  id: string;
}

export default class GetLifeguardDeviceUsage extends Request<DeviceUsage, LifeguardDeviceUsageOptions> {
  /**
   * Get the usage for a lifeguard device.
   * @param opts Parameters for the device/time period.
   */
  handle (opts: LifeguardDeviceUsageOptions): Promise<DeviceUsage> {
    return this.authReq({
      url: '/usage/lifeguard/device',
      method: 'GET',
      params: opts
    })
  }
}
