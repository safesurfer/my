import Request from '@/requests/request'

export interface RequestAmounts {
  /**
   * Amount of blocked requests over the week.
   */
  blocks: number[];
  /**
   * Amount of non-blocked requests over the week.
   */
  requests: number[];
}

export default class GetRequestAmounts extends Request<RequestAmounts, number> {
  /**
   * Get a summary of the amount of requests over
   * the last week.
   * @param timestamp Unix timestamp (seconds) in the user's locale.
   */
  handle (timestamp: number): Promise<RequestAmounts> {
    return this.authReq({
      url: '/usage/account/counts?start=' + encodeURIComponent(timestamp)
    })
  }
}
