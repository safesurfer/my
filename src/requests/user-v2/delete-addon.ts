import Request, { MetaResp } from "../request";

export default class DeleteAddon extends Request<MetaResp<null>, string> {
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/subscription/addons/' + opts,
      method: 'DELETE'
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
