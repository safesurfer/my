import Request from "../request"

export default class Get2FARegistrationQRCode extends Request<Blob, undefined> {
  handle(): Promise<Blob> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/qr',
      responseType: 'blob'
    })
  }
}