import Request, { MetaResp } from "../request";

export default class Get2FAStatus extends Request<MetaResp<number>, undefined> {
  handle(): Promise<MetaResp<number>> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/status',
      method: 'GET'
    })
  }
}
