import Request, { MetaResp } from "../request";

export default class GetChargebeeCheckoutRedirect extends Request<MetaResp<any>, undefined> {
  handle (): Promise<MetaResp<any>> {
    return this.authReq({
      url: '/v2/user/subscription/chargebee/redirect/checkout',
      method: 'GET'
    })
  }
}
