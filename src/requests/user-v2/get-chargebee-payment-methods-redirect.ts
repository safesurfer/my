import Request, { MetaResp } from "../request";

export default class GetChargebeePaymentMethodsRedirect extends Request<MetaResp<any>, undefined> {
  handle (): Promise<MetaResp<any>> {
    return this.authReq({
      url: '/v2/user/subscription/chargebee/redirect/payment-methods',
      method: 'GET'
    })
  }
}
