import Request, { MetaResp } from "../request"

export default class GetCommunicationConsent extends Request<MetaResp<boolean>, undefined> {
  /**
   * @returns Whether the user has consented to marketing comms.
   */
  handle (): Promise<MetaResp<boolean>> {
    return this.authReq({
      url: '/v2/user/communications-consent',
      method: 'GET'
    })
  }
}
