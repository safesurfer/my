import Request from "../request";

export default class GetQrSigninTokenImage extends Request<Blob, string> {
  handle(opts: string): Promise<Blob> {
    return this.authReq({
      url: '/v2/user/auth/qr-signin-tokens/' + opts + '/image',
      method: 'GET',
      responseType: 'blob'
    })
  }
}
