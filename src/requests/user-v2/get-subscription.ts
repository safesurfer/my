import Request, { MetaResp } from "../request";
import { Plan } from "./get-plan";

export interface GetSubscriptionArgs {
  refresh: boolean;
}

export interface ChargebeeSubscriptionAddon {
  id: string;
  quantity: number;
  updatable: boolean;
  minQuantity?: number;
  maxQuantity?: number;
  prorateOnUpgrade: boolean;
  prorateOnDowngrade: boolean;
  invoiceImmediately: boolean;
}

export interface ChargebeeSubscription {
  id: string;
  planId: string;
  createdAt: number;
  nextBillingAt: number;
  trialEnd: number;
  cancelledAt: number;
  cancelReason: string;
  currentTermEnd: number;
  startDate: number;
  customerCardStatus: string;
  status: string;
  addons: ChargebeeSubscriptionAddon[];
}

export interface GooglePlaySubscription {
  subscriptionID: string;
  purchaseToken: string;
  expiryTimestamp: number;
  autoRenewing: boolean;
  paymentState: number;
  cancelReason: number;
}

export type SubscriptionStatus = 'MISSING' | 'TRIAL' | 'TRIAL_CANCELLED' | 'ACTIVE' | 'NOT_PAID' | 'NO_CARD' | 'CANCELLED' | 'COURTESY' | 'NON_RENEWING' | 'INACTIVE_UNKNOWN' | 'FUTURE'

export interface Subscription {
  isActive: boolean;
  status: SubscriptionStatus;
  nextBill: number;
  canSubscribeChargebee: boolean;
  canSubscribeGooglePlay: boolean;
  chargebeeSubscription: ChargebeeSubscription;
  chargebeePlan: Plan;
  googlePlaySubscription: GooglePlaySubscription;
}

export default class GetSubscription extends Request<MetaResp<Subscription>, GetSubscriptionArgs> {
  public doLoginRedirect = true

  handle(opts: GetSubscriptionArgs): Promise<MetaResp<Subscription>> {
    return this.authReq({
      url: '/v2/user/subscription',
      method: 'GET',
      params: opts
    })
  }

  protected redirectOn401 (): boolean {
    return this.doLoginRedirect
  }
}
