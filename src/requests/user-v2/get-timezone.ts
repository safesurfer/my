import Request, { MetaResp } from "../request";

export default class GetTimezone extends Request<MetaResp<string>, undefined> {
  /**
   * @returns The user's timezone.
   */
  handle(): Promise<MetaResp<string>> {
    return this.authReq({
      url: '/v2/user/timezone',
      method: 'GET'
    })
  }
}
