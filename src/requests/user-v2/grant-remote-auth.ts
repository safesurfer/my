import Request, { MetaResp } from "../request";

export interface GrantArgs {
  key: string;
  role: string;
  pin: string;
  name: string;
}

export interface GrantResponse {
  deviceID: string;
  hmacSecret: string;
}

export default class GrantRemoteAuth extends Request<MetaResp<GrantResponse>, GrantArgs> {
  handle(opts: GrantArgs): Promise<MetaResp<GrantResponse>> {
    console.log(opts)
    return this.authReq({
      url: '/v2/user/auth/remote/request/grant',
      method: 'POST',
      data: this.encodeAsForm(opts)
    })
  }
}
