import Request, { MetaResp } from "../request";
import { CreateChargebeeAddonArgs } from "./post-subscription";
import { ChangeSubscriptionEstimate } from "./post-subscription-estimate";

export interface PostAddonEstimateArgs {
  id: string;
  delete: boolean;
  set?: CreateChargebeeAddonArgs;
}

export default class PostAddonEstimate extends Request<MetaResp<ChangeSubscriptionEstimate>, PostAddonEstimateArgs> {
  handle (opts: PostAddonEstimateArgs): Promise<MetaResp<ChangeSubscriptionEstimate>> {
    return this.authReq({
      url: '/v2/user/subscription/addons/' + opts.id + '/estimate',
      method: 'POST',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
