import { MetaResp } from "../request";
import { AddonModification } from "./post-addon-modification";
import Request from '@/requests/request'
import { ChangeSubscriptionEstimate } from "./post-subscription-estimate";

export default class PostAddonModification extends Request<MetaResp<ChangeSubscriptionEstimate>, AddonModification> {
  handle(opts: AddonModification): Promise<MetaResp<ChangeSubscriptionEstimate>> {
    return this.authReq({
      url: '/v2/user/subscription/addons/modify/estimate',
      method: 'POST',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
