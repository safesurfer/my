import qs from "qs";
import { APIAuthResponse } from "../account/get-auth";
import Request, { MetaResp } from "../request";

export interface PostAuthTokenArgs {
  username: string;
  password: string;
  'device-id': string;
  'enable-sub': boolean;
  roles?: string;
}

export default class PostAuthToken extends Request<MetaResp<APIAuthResponse>, PostAuthTokenArgs> {
  handle(opts: PostAuthTokenArgs): Promise<MetaResp<APIAuthResponse>> {
    return this.simpleReq({
      url: '/v2/user/auth/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: qs.stringify(opts)
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
