import Request, { MetaResp } from "../request";
import { ChangeSubscriptionEstimate } from "./post-subscription-estimate";
import { PutChargebeePlanArgs } from "./put-chargebee-plan";

export default class PostChangeChargebeePlanEstimate extends Request<MetaResp<ChangeSubscriptionEstimate>, PutChargebeePlanArgs> {
  handle(opts: PutChargebeePlanArgs): Promise<MetaResp<ChangeSubscriptionEstimate>> {
    return this.authReq({
      url: '/v2/user/subscription/chargebee/plan/estimate',
      method: 'POST',
      data: opts
    })
  }

  protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
