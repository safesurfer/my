import Request, { MetaResp } from "../request";

export default class PostData extends Request<MetaResp<null>, {[key: string]: string}> {
  handle(opts: { [key: string]: string; }): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/data',
      method: 'POST',
      data: opts
    })
  }
}
