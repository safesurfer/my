import Request, { MetaResp } from "../request"

export interface PostQrSigninTokenArgs {
  roles: string[];
  metadata: {[key: string]: string};
}

export interface QrSigninToken {
  token: string;
  expiry: number;
}

export default class PostQrSigninToken extends Request<MetaResp<QrSigninToken>, PostQrSigninTokenArgs> {
  handle(opts: PostQrSigninTokenArgs): Promise<MetaResp<QrSigninToken>> {
    const form: any = {
      roles: opts.roles
    }
    Object.keys(opts.metadata).forEach(key => {
      form['metadata-' + key] = opts.metadata[key]
    })
    return this.authReq({
      url: '/v2/user/auth/qr-signin-tokens',
      method: 'POST',
      data: this.encodeAsForm(form)
    })
  }
}
