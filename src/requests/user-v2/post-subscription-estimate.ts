import { Quota } from "../quotas/get-quota";
import Request, { MetaResp } from "../request";
import { CreateChargebeeSubscriptionArgs } from "./post-subscription";

export interface InvoiceSummaryItem {
  name: string;
  cost: number;
  discount: number;
}

export interface CouponValidUntil {
  period: number;
  periodUnit: string;
}

export interface InvoiceSummary {
  total: number;
  totalDiscount: number;
  items: InvoiceSummaryItem[];
  discountValidOnce: boolean;
  discountValidUntil: CouponValidUntil;
}

export interface ChangeSubscriptionEstimate {
  immediateCosts?: InvoiceSummary;
  nextRenewalCosts?: InvoiceSummary;
  unbilledCharges?: InvoiceSummary;
  quotas: {[quotaName: string]: Quota};
  trialEnd: boolean;
  subscriptionStart: boolean;
}

export default class PostSubscriptionEstimate extends Request<MetaResp<ChangeSubscriptionEstimate>, CreateChargebeeSubscriptionArgs> {
  handle(opts: CreateChargebeeSubscriptionArgs): Promise<MetaResp<ChangeSubscriptionEstimate>> {
    return this.authReq({
      url: '/v2/user/subscription/estimate',
      method: 'POST',
      data: opts
    })
  }

  /**
   * Given an axios error and promise reject function, reject
   * the promise. Default behavior to reject with error code.
   * @param err The axios error.
   * @param reject Reject function.
   */
   protected rejector (err: any, reject: (reason?: any) => void) {
    if (err.response && err.response.data) {
      reject(err.response.data)
    } else {
      reject() // eslint-disable-line
    }
  }
}
