import Request, { MetaResp } from "../request";

export interface Put2FAStatusArgs {
  status: number;
  token: string;
}

export interface Put2FAStatusResp {
  expiry: number;
  secret: string;
}

export default class Put2FAStatus extends Request<MetaResp<Put2FAStatusResp>, Put2FAStatusArgs> {
  handle(opts: Put2FAStatusArgs): Promise<MetaResp<Put2FAStatusResp>> {
    return this.authReq({
      url: '/v2/user/auth/twofactor/status',
      method: 'PUT',
      data: this.encodeAsForm(opts)
    })
  }
}