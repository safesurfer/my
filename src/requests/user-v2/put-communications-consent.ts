import Request, { MetaResp } from "../request"

export default class PutCommunicationsConsent extends Request<MetaResp<null>, boolean> {
  /**
   * Give consent (or not) for marketing comms.
   * @param opts Whether to give consent.
   */
  handle (opts: boolean | null): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/communications-consent',
      method: 'PUT',
      data: opts
    })
  }
}
