import Request, { MetaResp } from "../request";

export default class PutTimezone extends Request<MetaResp<null>, string> {
  /**
   * Set the user's timezone.
   * @param opts The timezone.
   */
  handle(opts: string): Promise<MetaResp<null>> {
    return this.authReq({
      url: '/v2/user/timezone',
      method: 'PUT',
      data: '"' + opts + '"'
    })
  }
}
