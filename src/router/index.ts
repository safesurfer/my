import Vue from 'vue'
import VueRouter from 'vue-router'
import { getLoginRoute, hasValidAuth } from '@/helpers/functions'
import Login from '@/views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'AccountHome',
    component: () => import('@/views/AccountHome.vue'),
    meta: {
      title: 'Home'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      noAuthAccess: true,
      title: 'Log in'
    }
  },
  {
    path: '/account-settings',
    name: 'AccountSettings',
    component: () => import('@/views/AccountSettings.vue'),
    meta: {
      title: 'Account settings'
    }
  },
  {
    path: '/alerts',
    name: 'Alerts',
    component: () => import('@/views/Alerts.vue'),
    meta: {
      title: 'Alerts',
      help: [
        'The Alerts menu shows notable activity for protected devices, such as attempts to view blocked sites. It\'s good to keep in mind that some services (such as social media apps) are spread throughout the Internet. If something is being blocked in cases where the device user doesn\'t use the service in question, it\'s possible that it\'s being blocked correctly in the background (such as a shared social media post on an unrelated website).',
        'You can manage scheduled email alerts by clicking the Emails button.'
      ]
    }
  },
  {
    path: '/alert-settings',
    name: 'AlertSettings',
    component: () => import('@/views/AlertSettings.vue'),
    meta: {
      title: 'Alert emails',
      help: [
        'Deliver alerts to your inbox.',
        'We\'ve set you up with emails we think you\'ll find useful. You can turn them off, or customize with more specific settings.'
      ]
    }
  },
  {
    path: '/device',
    name: 'Device',
    component: () => import('@/views/Device.vue'),
    meta: {
      title: 'Device'
    }
  },
  {
    path: '/devices',
    name: 'Devices',
    component: () => import('@/views/Devices.vue'),
    meta: {
      title: 'Devices',
      help: [
        'A device will turn up in this list once you\'ve protected it. From there, you can edit its settings or view activity.',
        'To get started, click the New device option.' 
      ]
    }
  },
  {
    path: '/device-setup',
    name: 'DeviceSetup',
    component: () => import('@/views/DeviceSetup.vue'),
    meta: {
      title: 'Device setup',
    }
  },
  {
    path: '/block-categories',
    name: 'BlockCategories',
    component: () => import('@/views/EditBlockedCategories.vue'),
    meta: {
      title: 'Blocking',
      help: [
        'The Blocking menu lets you block different categories of websites and apps for individual devices.',
        'Click the the blue bubble found below to get started. The Defaults option covers block settings for new devices that you add to your account. If you customise settings for a specific device, it will then ignore the Defaults list from that point onwards.',
        'If you select social media services to be blocked, please note that this works best on Android, Windows, and Mac devices.'
      ]
    }
  },
  {
    path: '/usage',
    name: 'Usage',
    component: () => import('@/views/Usage.vue'),
    meta: {
      title: 'Browsing history',
      help: [
        'Once you have protected a device with Safe Surfer, you\'ll be able to review the websites it has attempted to access. Don\'t worry if you see lots of computer-related domains, or if you see requests happening overnight, which is normal.',
        'Social media posts can often be found within news articles on unrelated websites, so blocked attempts to access a social media service may not always be deliberate.'
      ]
    }
  },
  {
    path: '/diagnostics',
    name: 'Diagnostics',
    component: () => import('@/views/Diagnostics.vue'),
    meta: {
      title: 'Diagnostics'
    }
  },
  {
    path: '/screencasts',
    name: 'Screencasts',
    component: () => import('@/views/Screencasts.vue'),
    meta: {
      title: 'Nudity detection'
    }
  },
  {
    path: '/screencasts/events',
    name: 'ScreencastEvents',
    component: () => import('@/views/ScreencastEvents.vue'),
    meta: {
      title: 'Content detections'
    }
  },
  {
    path: '/vpn-alerts',
    name: 'VPNAlerts',
    component: () => import('@/views/VPNAlerts.vue'),
    meta: {
      title: 'VPN Alerts'
    }
  },
  {
    path: '/verify',
    name: 'Verify',
    component: () => import('@/views/Verify.vue'),
    meta: {
      noAuthAccess: true,
      title: 'Verify Account'
    }
  },
  {
    path: '/send-password-reset',
    redirect: {
      name: 'Login',
      query: {
        'auth-steps': JSON.stringify([0, 2]),
        'auth-step': '1'
      }
    }
  },
  {
    path: '/confirm-pass-reset',
    name: 'ConfirmPassReset',
    component: () => import('@/views/ConfirmPasswordReset.vue'),
    meta: {
      noAuthAccess: true,
      title: 'Password Reset'
    }
  },
  {
    path: '/block-standalone',
    name: 'BlockStandalone',
    component: () => import('@/views/BlockStandalone.vue'),
    meta: {
      noAuthAccess: true,
      hideHeaderFooter: true
    }
  },
  {
    path: '/create-account',
    name: 'CreateAccount',
    component: () => import('@/views/CreateAccount.vue'),
    meta: {
      noAuthAccess: true,
      title: 'Create Account'
    }
  },
  {
    path: '/notification-log',
    name: 'NotificationLog',
    component: () => import('@/views/NotificationLog.vue'),
    meta: {
      title: 'Texts & social media'
    }
  },
  {
    path: '/notification-log/notification',
    name: 'LoggedNotification',
    component: () => import('@/views/LoggedNotification.vue'),
    meta: {
      title: 'Logged Notification'
    }
  },
  {
    path: '/clear-history',
    name: 'ClearHistory',
    component: () => import('@/views/ClearHistory.vue'),
    meta: {
      title: 'Clear history'
    }
  },
  {
    path: '/screentime/timers',
    name: 'ScreentimeTimers',
    component: () => import('@/views/ScreentimeTimers.vue'),
    meta: {
      title: 'Timers',
      help: [
        'Timers let you restrict access to the internet, or particular sites, for an amount of time per day.',
        'The user of the device can start and stop their access timer when they please.'
      ]
    }
  },
  {
    path: '/screentime/timetables',
    name: 'ScreentimeTimetables',
    component: () => import('@/views/ScreentimeTimetables.vue'),
    meta: {
      title: 'Timetables',
      help: [
        'Timetables let you restrict access to the internet, or particular sites, at particular times of the day or week.',
        'They\'re great for blocking addictive content that may otherwise eat into sleep or work time.'
      ]
    }
  },
  {
    path: '/self-service',
    name: 'SelfService',
    component: () => import('@/views/SelfService.vue'),
    meta: {
      hideHeaderFooter: true,
      noAuthAccess: true,
      title: 'My access'
    }
  },
  {
    path: '/screentime/stop-start-access',
    name: 'StopStartAccess',
    component: () => import('@/views/StopStartAccess.vue'),
    meta: {
      title: 'Pause internet',
      help: [
        'On this screen, you can override the rules you\'ve made inside "timers", "timetables", or "blocking" for a certain amount of time.',
        'This is useful to reward or deny screentime according to the situation.'
      ]
    }
  },
  {
    path: '/twofactor/enrolment',
    name: 'TwofactorEnrolment',
    component: () => import('@/views/TwofactorEnrolment.vue'),
    meta: {
      title: 'Enable Two Factor Authorization'
    }
  },
  {
    path: '/device-registration',
    name: 'DeviceRegistration',
    component: () => import('@/views/DeviceRegistration.vue'),
    meta: {
      title: 'Device Registration',
      noAuthAccess: true,
      hideHeaderFooter: true
    }
  },
  {
    path: '/device-reauth',
    name: 'DeviceReauth',
    component: () => import('@/views/DeviceReauth.vue'),
    meta: {
      title: 'Device Reauth',
      noAuthAccess: true,
      hideHeaderFooter: true
    }
  },
  {
    path: '/router-registration',
    name: 'RouterRegistration',
    component: () => import('@/views/RouterRegistration.vue'),
    meta: {
      title: 'Lifeguard Registration',
      hideHeaderFooter: true,
      noAuthAccess: true
    }
  },
  {
    path: '/windows-registration',
    name: 'WindowsRegistration',
    component: () => import('@/views/WindowsRegistration.vue'),
    meta: {
      title: 'Windows Registration',
      hideHeaderFooter: true,
      noAuthAccess: true
    }
  },
  {
    path: '/profile-setup',
    name: 'ProfileSetup',
    component: () => import('@/views/ProfileSetup.vue'),
    meta: {
      title: 'Profile Setup',
      hideHeaderFooter: true,
      noAuthAccess: true
    }
  },
  {
    path: '/security-alerts',
    name: 'SecurityAlerts',
    component: () => import('@/views/SecurityAlerts.vue'),
    meta: {
      title: 'Security Alerts'
    }
  },
  {
    path: '/app-approval',
    name: 'AppApproval',
    component: () => import('@/views/AppApproval.vue'),
    meta: {
      title: 'App approval',
      help: [
        'When App approval is enabled, newly installed apps will be blocked until they are approved here.',
      ]
    }
  },
  {
    path: '/location',
    name: 'Location',
    component: () => import('@/views/Location.vue'),
    meta: {
      title: 'Location History'
    }
  },
  {
    path: '/kse-promo-switch',
    name: 'KsePromoSwitch',
    component: () => import('@/views/KsePromoSwitch.vue'),
    meta: {
      title: 'Switch Plans'
    }
  },
  {
    path: '/ios-prevent-uninstall',
    name: 'IosPreventUninstall',
    component: () => import('@/views/IosPreventUninstall.vue'),
    meta: {
      title: 'Prevent Uninstall'
    }
  },
  {
    path: '/supervision-complete',
    name: 'IosSupervisionComplete',
    component: () => import('@/views/IosSupervisionComplete.vue'),
    meta: {
      title: 'Supervision Complete'
    },
  },
  {
    path: '/ios-unsupervise',
    name: 'IosUnSupervise',
    component: () => import('@/views/IosUnSupervise.vue'),
    meta: {
      title: 'Unsupervise Device'
    }
  },
  {
    path: '/unsupervision-complete',
    name: 'IosUnSupervisionComplete',
    component: () => import('@/views/IosUnSupervisionComplete.vue'),
    meta: {
      title: 'Un-supervision Complete'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

// Guard to redirect to login if we don't have valid auth
router.beforeEach((to, from, next) => {
  if (to && from && to.name && from.name && to.name !== from.name) {
    window.scrollTo(0, 0)
  }
  if (to.meta && !to.meta.noAuthAccess && !hasValidAuth()) {
    // Redirect to login
    next(getLoginRoute(to, to.fullPath))
  } else {
    // Proceed
    next()
  }
})

// Update page title
const BASE_TITLE = 'My Safe Surfer'
router.afterEach((to) => {
  Vue.nextTick(() => {
    // Construct the URL
    document.title = (to.name === 'Login' || to.name === 'AccountHome') 
      ? 'My Safe Surfer' 
      : (to.meta && to.meta.title ? to.meta.title + ' · ' + BASE_TITLE : BASE_TITLE)
  })
})

export default router
