const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require('webpack')

module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      fallback: {
        "http": require.resolve("stream-http"),
        "https": require.resolve("https-browserify")
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        process: 'process/browser',
      }),
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
      // new BundleAnalyzerPlugin(),
    ],
  },
  pwa: {
    name: 'My Safe Surfer',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: './src/sw.js'
    },
    themeColor: '#1468B1'
  },
  // Enable the below for HTTPS
  devServer: {
    host: '0.0.0.0',
    port: 8080, // CHANGE YOUR PORT HERE!
  },
}